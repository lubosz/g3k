#!/usr/bin/env python3

# generate_layouts.py
#
# Copyright 2021 Remco Kranenburg <remco@burgsoft.nl>
# SPDX-License-Identifier: MIT
#
# This script generates keyboard layouts in a format expected by G3kKeyboard
# from unicode CLDR data that can be found here:
#
#     http://cldr.unicode.org/
#     https://github.com/unicode-org/cldr
#
# The first argument is the input directory, which should be the path to
# cldr/keyboards/android. The second argument is the output directory, which
# should be the path to res/keyboard-layouts.
#
# Example usage:
#
#   scripts/generate_layouts.py ../cldr/keyboards/android res/keyboard-layouts
#

import re
from json import dump
from pathlib import Path
from sys import argv
from xml.etree import ElementTree


ANDROID_KEYBOARD_SUFFIX = "-t-k0-android.xml"

BACKSPACE_BUTTON = dict(label="⌫", keyval=0xff08, x=8.5, y=2.0, w=1.5)
RETURN_BUTTON = dict(label="↵", character="\n", x=8.5, y=3.0, w=1.5)
EMOJI_BUTTON = dict(label="☺", target="Emoji")  # unpositioned


class UnknownModeError(Exception):
    """Error raised when encountering an unknown keyboard mode"""


def removesuffix(s, suffix):
    # TODO: can be removed when minimum Python support is Python 3.9
    if suffix and s.endswith(suffix):
        return s[:-len(suffix)]
    else:
        return s[:]


def get_mode_name(key_map):
    if not "modifiers" in key_map.attrib:
        return "Main"
    else:
        modifiers = key_map.attrib["modifiers"]

        if modifiers == "shift caps":
            return "Capitals"
        elif modifiers == "opt":
            return "Numbers"
        elif modifiers == "opt+shift":
            return "Special"
        else:
            raise UnknownModeError(modifiers)


def get_x(i, num_keys, space_in_row):
    """
    Character keys are centered in the row
    """
    empty_space = max(0, space_in_row - num_keys)
    filled_space = space_in_row - empty_space
    return empty_space / 2 + filled_space / num_keys * i


def get_w(num_keys, space_in_row):
    """
    Character keys are at most 1.0, but can shrink to fit more keys.
    """
    return min(1.0, space_in_row / num_keys)


def get_character(k):
    def hex_to_char(x):
        return chr(int(x[1], 16))

    return re.sub(r"\\u{(\w+)}", hex_to_char, k.attrib["to"])


def get_shift_button(mode_name):
    if mode_name == "Main":
        label = "⇧"
        target = "Capitals"
    elif mode_name == "Capitals":
        label = "⇧"
        target = "Main"
    elif mode_name == "Numbers":
        label = "=\\<"
        target = "Special"
    elif mode_name == "Special":
        label = "?123"
        target = "Numbers"
    else:
        raise UnknownModeError(mode_name)

    return dict(label=label, x=0.0, y=2.0, w=1.5, target=target)


def get_numbers_button(mode_name):
    if mode_name in ["Main", "Capitals"]:
        label = "?123"
        target = "Numbers"
    elif mode_name in ["Numbers", "Special", "Emoji"]:
        label = "ABC"
        target = "Main"
    else:
        raise UnknownModeError(mode_name)

    return dict(label=label, x=0.0, y=3.0, w=1.5, target=target)


def generate_last_row(mode_name, fourth_row_map):
    """
    The last row is a bit complicated, because it optionally has a space button
    that should take up all remaining space and there are fixed buttons mixed
    with a variable number of internationalized buttons.

    Layout:

    [?123] (buttons...) [☺] [Space] (buttons...) [ ↵ ]

    The left-most button is the fixed Numbers mode toggle button. Then come all
    the internationalized buttons. If a Space button exists, the Emoji mode
    toggle button is inserted directly left of it, otherwise directly right of
    the Numbers mode toggle button. Finally, the Return button is appended.

    The Numbers mode toggle and Return button have a fixed width of 1.5. The
    other buttons divide up the remaining space, but have a maximum width of
    1.0.

    TODO: If there is remaining space, the Space button is expanded. If there
          is no space button, the non-fixed buttons are centered, leaving a
          small gap between them and the fixed buttons.
    """
    space_button_index = -1
    for i, k in enumerate(fourth_row_map):
        if get_character(k) == " ":
            space_button_index = i

    variable_buttons = [dict(character=get_character(k))
                        for k in fourth_row_map]

    if mode_name in ["Main", "Capitals"]:
        if space_button_index >= 0:
            variable_buttons.insert(space_button_index, EMOJI_BUTTON)
        else:
            variable_buttons.insert(0, EMOJI_BUTTON)

    if space_button_index >= 0 and len(variable_buttons) <= 7:
        space_button_w = 7.0 - (len(variable_buttons) - 1)
        x = 1.5

        for k in variable_buttons:
            if "character" in k and k["character"] == " ":
                w = space_button_w
            else:
                w = 1.0

            k["x"] = x
            k["y"] = 3.0
            k["w"] = w
            x += w
    else:
        for i, k in enumerate(variable_buttons):
            k["x"] = 1.5 + get_x(i, len(variable_buttons), 7.0)
            k["y"] = 3.0
            k["w"] = get_w(len(variable_buttons), 7.0)

    return [get_numbers_button(mode_name), *variable_buttons, RETURN_BUTTON]


def generate_emoji_mode():
    return dict(
        name="Emoji",
        keys=[
            dict(character="😀", x=0.0, y=0.0, w=1.0),
            dict(character="😃", x=1.0, y=0.0, w=1.0),
            dict(character="😄", x=2.0, y=0.0, w=1.0),
            dict(character="😁", x=3.0, y=0.0, w=1.0),
            dict(character="😆", x=4.0, y=0.0, w=1.0),
            dict(character="😅", x=5.0, y=0.0, w=1.0),
            dict(character="🤣", x=6.0, y=0.0, w=1.0),
            dict(character="😂", x=7.0, y=0.0, w=1.0),
            dict(character="🙂", x=8.0, y=0.0, w=1.0),
            dict(character="🙃", x=9.0, y=0.0, w=1.0),
            dict(character="😉", x=0.0, y=1.0, w=1.0),
            dict(character="😊", x=1.0, y=1.0, w=1.0),
            dict(character="😊", x=2.0, y=1.0, w=1.0),
            dict(character="😇", x=3.0, y=1.0, w=1.0),
            dict(character="🥰", x=4.0, y=1.0, w=1.0),
            dict(character="😍", x=5.0, y=1.0, w=1.0),
            dict(character="🤩", x=6.0, y=1.0, w=1.0),
            dict(character="😘", x=7.0, y=1.0, w=1.0),
            dict(character="😗", x=8.0, y=1.0, w=1.0),
            dict(character="☺", x=9.0, y=1.0, w=1.0),
            dict(character="😚", x=0.0, y=2.0, w=1.0),
            dict(character="😙", x=1.0, y=2.0, w=1.0),
            dict(character="🥲", x=2.0, y=2.0, w=1.0),
            dict(character="😋", x=3.0, y=2.0, w=1.0),
            dict(character="😛", x=4.0, y=2.0, w=1.0),
            dict(character="😜", x=5.0, y=2.0, w=1.0),
            dict(character="🤪", x=6.0, y=2.0, w=1.0),
            dict(character="😝", x=7.0, y=2.0, w=1.0),
            dict(character="🤑", x=8.0, y=2.0, w=1.0),
            dict(character="🤗", x=9.0, y=2.0, w=1.0),
            get_numbers_button("Emoji"),
            dict(character="🤭", x=1.5, y=3.0, w=1.0),
            dict(character="🤫", x=2.5, y=3.0, w=1.0),
            dict(character="🤔", x=3.5, y=3.0, w=1.0),
            dict(character="🤐", x=4.5, y=3.0, w=1.0),
            dict(character="🤨", x=5.5, y=3.0, w=1.0),
            dict(character="😐", x=6.5, y=3.0, w=1.0),
            dict(character="😑", x=7.5, y=3.0, w=1.0),
            RETURN_BUTTON,
        ]
    )


def convert(language_file, locale):
    tree = ElementTree.parse(language_file)
    root = tree.getroot()

    result = dict(name=locale, modes=[])

    for key_map in root.findall("keyMap"):
        mode_name = get_mode_name(key_map)
        rows = [
            [
                dict(character=get_character(k))
                for k in key_map
                if k.attrib["iso"].startswith("D")
            ],
            [
                dict(character=get_character(k))
                for k in key_map
                if k.attrib["iso"].startswith("C")
            ],
            [
                dict(character=get_character(k))
                for k in key_map
                if k.attrib["iso"].startswith("B")
            ],
        ]

        # set key positions of first row
        for i, k in enumerate(rows[0]):
            k["x"] = get_x(i, len(rows[0]), 10.0)
            k["y"] = 0.0
            k["w"] = get_w(len(rows[0]), 10.0)

        # set key positions of second row
        for i, k in enumerate(rows[1]):
            k["x"] = get_x(i, len(rows[1]), 10.0)
            k["y"] = 1.0
            k["w"] = get_w(len(rows[1]), 10.0)

        # set key positions of third row
        for i, k in enumerate(rows[2]):
            k["x"] = 1.5 + get_x(i, len(rows[2]), 7.0)
            k["y"] = 2.0
            k["w"] = get_w(len(rows[2]), 7.0)

        # add fixed buttons of third row
        rows[2].insert(0, get_shift_button(mode_name))
        rows[2].append(BACKSPACE_BUTTON)

        # generate fourth row
        fourth_row_map = [k for k in key_map if k.attrib["iso"].startswith("A")]
        rows.append(generate_last_row(mode_name, fourth_row_map))

        keys = [k for row in rows for k in row]  # flatten list of keys
        result["modes"].append(dict(name=mode_name, keys=keys))

    result["modes"].append(generate_emoji_mode())

    return result


def to_locale(filename):
  language_part = removesuffix(filename, ANDROID_KEYBOARD_SUFFIX)
  return language_part.replace("-", "_")


if __name__ == "__main__":
    if not len(argv) == 3:
        print(f"Usage: {argv[0]} <input_directory> <output_directory>")
        print()
        print("Generate keyboard layouts in a format expected by G3kKeyboard")
        print("from unicode CLDR data.")
        print()
        print(" - Input directory should be path to cldr/keyboards/android")
        print(" - Output directory should be path to res/keyboard-layouts")
        exit(1)

    input_directory = Path(argv[1])
    output_directory = Path(argv[2])

    output_directory.mkdir(parents=True, exist_ok=True)

    generated_files = []

    for language_file in input_directory.iterdir():
        if language_file.name.endswith(ANDROID_KEYBOARD_SUFFIX):
            locale = to_locale(language_file.name)
            output = convert(language_file, locale)

            output_file = output_directory / f"{locale}.json"

            with open(output_file, "w", encoding="utf-8") as f:
                dump(output, f, indent=2, ensure_ascii=False)

            generated_files.append(output_file)

    resource_file = output_directory / "keyboard-layouts.gresource.xml"

    with open(resource_file, "w", encoding="utf-8") as f:
        print("""<?xml version="1.0" encoding="UTF-8"?>""", file=f)
        print("""<gresources>""", file=f)
        print("""  <gresource prefix="/keyboard-layouts">""", file=f)

        for file in generated_files:
            print(f"""    <file>{file.name}</file>""", file=f)

        print("""  </gresource>""", file=f)
        print("""</gresources>""", file=f)
