/*
 * gulkan
 * Copyright 2018-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

layout (location = 0) in vec4 in_world_position;
layout (location = 1) in vec3 in_eye_position;
#ifdef HAVE_COLOR
layout (location = 2) in vec3 in_color;
#endif
#ifdef HAVE_NORMAL
layout (location = 3) in vec3 in_world_normal;
#endif
#ifdef HAVE_TANGENT
layout (location = 4) in vec3 in_world_tangent;
#endif
#ifdef HAVE_TEXCOORD_0
layout (location = 5) in vec2 in_uv;
#endif

layout (binding = 1) uniform Material
{
  vec4  diffuse_color;
  vec4  emissive_color;
  vec2  normal_scale;
  float ao_map_intensity;
  float emissive_intensity;
  float color_alpha;
  float roughness;
  float metalness;
}
material;

#ifdef HAVE_DIFFUSE_MAP
layout (binding = 2) uniform sampler2D diffuse_map;
#endif
#ifdef HAVE_NORMAL_MAP
layout (binding = 3) uniform sampler2D normal_map;
#endif
#ifdef HAVE_METALLIC_ROUGHNESS_MAP
layout (binding = 4) uniform sampler2D metallic_roughness_map;
#endif
#ifdef HAVE_EMISSIVE_MAP
layout (binding = 5) uniform sampler2D emissive_map;
#endif
#ifdef HAVE_AO_MAP
layout (binding = 6) uniform sampler2D ao_map;
#endif

layout (location = 0) out vec4 out_color;

const vec3 lights[3] = {
  vec3 (0.0, 10.0, 0.0),
  vec3 (-10.0, 10.0, -10.0),
  vec3 (10.0, 10.0, 10.0),
};
const vec3  specular_color = vec3 (1.0, 1.0, 1.0);
const float shininess = 16.0;

const float PI = 3.14159265359;

// Normal Distribution function --------------------------------------
float
D_GGX (float dotNH, float roughness)
{
  float alpha = roughness * roughness;
  float alpha2 = alpha * alpha;
  float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
  return (alpha2) / (PI * denom * denom);
}

// Geometric Shadowing function --------------------------------------
float
G_SchlicksmithGGX (float dotNL, float dotNV, float roughness)
{
  float r = (roughness + 1.0);
  float k = (r * r) / 8.0;
  float GL = dotNL / (dotNL * (1.0 - k) + k);
  float GV = dotNV / (dotNV * (1.0 - k) + k);
  return GL * GV;
}

// Fresnel function ----------------------------------------------------
vec3
F_Schlick (float cosTheta, float metallic)
{
  vec4 diffuse_color;
#if defined(HAVE_TEXCOORD_0) && defined(HAVE_DIFFUSE_MAP)
  diffuse_color = texture (diffuse_map, in_uv);
#else
  diffuse_color = material.diffuse_color;
#endif

  vec3 F0 = mix (vec3 (0.04), diffuse_color.rgb,
                 metallic); // * material.specular
  vec3 F = F0 + (1.0 - F0) * pow (1.0 - cosTheta, 5.0);
  return F;
}

// Specular BRDF composition --------------------------------------------

vec3
BRDF (vec3 L, vec3 V, vec3 N, float metallic, float roughness)
{
  // Precalculate vectors and dot products
  vec3 H = normalize (V + L);

  float dotNL = max (0.0, dot (N, L));
  float dotNV = max (0.001, dot (N, V));
  float dotNH = max (0.001, dot (N, H));
  float dotLH = max (0.001, dot (L, H));

  // Light color fixed
  vec3 lightColor = vec3 (1.0);

  vec3 color = vec3 (0.0);

  if (dotNL > 0.0)
    {
      float rroughness = max (0.05, roughness);
      // D = Normal distribution (Distribution of the microfacets)
      float D = D_GGX (dotNH, roughness);
      // G = Geometric shadowing term (Microfacets shadowing)
      float G = G_SchlicksmithGGX (dotNL, dotNV, rroughness);
      // F = Fresnel factor (Reflectance depending on angle of incidence)
      vec3 F = F_Schlick (dotNV, metallic);

      vec3 spec = D * F * G / (4.0 * dotNL * dotNV);

      color += spec * dotNL * lightColor;
    }

  return color;
}

void
main ()
{
  vec3 N;
  out_color = vec4 (0);
#ifdef HAVE_NORMAL
  vec3 normal = normalize (in_world_normal);
#endif

#if defined(HAVE_TEXCOORD_0) && defined(HAVE_NORMAL_MAP)
  vec3 normal_map_sample = texture (normal_map, in_uv).rgb;
  vec3 uncompressed_normal = 2.0f * normal_map_sample - 1.0f;
  N = normal;

#if !defined(HAVE_TANGENT)
  vec3 Q1 = dFdx (in_world_position.xyz);
  vec3 Q2 = dFdy (in_world_position.xyz);
  vec2 st1 = dFdx (in_uv);
  vec2 st2 = dFdy (in_uv);
  vec3 in_world_tangent = normalize (Q1 * st2.t - Q2 * st1.t);
#endif

  vec3 T = normalize (in_world_tangent);

  // re-orthogonalize T with respect to N
  T = normalize (T - dot (T, N) * N);

  vec3 B = normalize (cross (N, T));

  // mat3 TBN = transpose(mat3(T, B, N));
  mat3 TBN = mat3 (T, B, N);

  normal = normalize (TBN * uncompressed_normal);
#endif

  vec4 diffuse_color;
#if defined(HAVE_TEXCOORD_0) && defined(HAVE_DIFFUSE_MAP)
  diffuse_color = texture (diffuse_map, in_uv);
#else
  diffuse_color = material.diffuse_color;
#endif

#if defined(HAVE_TEXCOORD_0) && defined(HAVE_AO_MAP)
  float ambient = texture (ao_map, in_uv).r;
#else
  float ambient = 1.0;
#endif

#ifdef HAVE_NORMAL
  N = normal;
  vec3 V = normalize (in_eye_position - in_world_position.xyz);

  vec3 Lo = vec3 (0.0);

#if defined(HAVE_TEXCOORD_0) && defined(HAVE_METALLIC_ROUGHNESS_MAP)
  float roughness = texture (metallic_roughness_map, in_uv).g;
  float metalness = texture (metallic_roughness_map, in_uv).b;
#else
  float roughness = material.roughness;
  float metalness = material.metalness;
#endif

  for (int i = 0; i < 3; i++)
    {
      vec3 L = normalize (lights[i] - in_world_position.xyz);
      Lo += BRDF (L, V, N, metalness, roughness);
    }

  out_color = vec4 (Lo + diffuse_color.rgb * ambient, 1.0);
#else
  out_color = diffuse_color * ambient;
#endif

#if defined(HAVE_TEXCOORD_0) && defined(HAVE_EMISSIVE_MAP)
  out_color.rgb += texture (emissive_map, in_uv).rgb;
#endif
}
