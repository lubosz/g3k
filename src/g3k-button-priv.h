/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_BUTTON_PRIV_H_
#define G3K_BUTTON_PRIV_H_

#include <glib.h>
#include <graphene.h>

#include "g3k-button.h"

gboolean
g3k_button_initialize (G3kButton       *self,
                       G3kContext      *g3k,
                       graphene_size_t *size_meters);

#endif
