/*
 * Graphene Extensions
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_BUTTON_H_
#define G3K_BUTTON_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-plane.h"

G_BEGIN_DECLS

struct _G3kButtonClass
{
  G3kPlaneClass parent;
};

#define G3K_TYPE_BUTTON g3k_button_get_type ()
G_DECLARE_DERIVABLE_TYPE (G3kButton, g3k_button, G3K, BUTTON, G3kPlane)

G3kButton *
g3k_button_new (G3kContext *g3k, graphene_size_t *size_meters);

void
g3k_button_set_text (G3kButton *self, int label_count, gchar **label);

void
g3k_button_set_icon (G3kButton *self, const gchar *url);

void
g3k_button_set_color (G3kButton *self, const graphene_vec4_t *color);
void
g3k_button_set_selection_color (G3kButton *self, gboolean is_selected);

void
g3k_button_reset_color (G3kButton *self);

G_END_DECLS

#endif /* G3K_BUTTON_H_ */
