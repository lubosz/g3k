/*
 * g3k
 * Copyright 2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_CONTEXT_H_
#define G3K_CONTEXT_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>
#include <gxr.h>

G_BEGIN_DECLS

#define G3K_TYPE_CONTEXT g3k_context_get_type ()
G_DECLARE_FINAL_TYPE (G3kContext, g3k_context, G3K, CONTEXT, GObject)

#ifndef __GTK_DOC_IGNORE__
typedef struct _G3kRenderer      G3kRenderer;
typedef struct _G3kController    G3kController;
typedef struct _G3kObject        G3kObject;
typedef struct _G3kObjectManager G3kObjectManager;
#endif

G3kContext *
g3k_context_new (void);

G3kContext *
g3k_context_new_from_gxr (GxrContext *gxr);

G3kContext *
g3k_context_new_from_vulkan_extensions (GSList *instance_ext_list,
                                        GSList *device_ext_list);

G3kContext *
g3k_context_new_full (GSList  *instance_ext_list,
                      GSList  *device_ext_list,
                      char    *app_name,
                      uint32_t app_version);

GxrContext *
g3k_context_get_gxr (G3kContext *self);

GulkanContext *
g3k_context_get_gulkan (G3kContext *self);

G3kRenderer *
g3k_context_get_renderer (G3kContext *self);

graphene_matrix_t *
g3k_context_get_views (G3kContext *self);

graphene_matrix_t *
g3k_context_get_projections (G3kContext *self);

graphene_matrix_t *
g3k_context_get_vps (G3kContext *self);

void
g3k_context_render (G3kContext *self);

VkImageLayout
g3k_context_get_upload_layout (G3kContext *self);

G3kController *
g3k_context_get_controller (G3kContext *self, GxrController *gxr_controller);

GList *
g3k_context_get_controllers (G3kContext *self);

G3kController *
g3k_context_init_dummy_controller (G3kContext *self);

G3kController *
g3k_context_get_primary_controller (G3kContext *self);

void
g3k_context_make_controller_primary (G3kContext    *self,
                                     G3kController *controller);

gboolean
g3k_context_is_controller_primary (G3kContext *self, G3kController *controller);

G3kObject *
g3k_context_get_root (G3kContext *self);

G3kObjectManager *
g3k_context_get_manager (G3kContext *self);

float
g3k_context_get_ms_since_last_poll (G3kContext *self);

G_END_DECLS

#endif /* G3K_CONTEXT_H_ */
