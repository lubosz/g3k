/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_KEYBOARD_BUTTON_H_
#define G3K_KEYBOARD_BUTTON_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>

#include "g3k-button.h"

G_BEGIN_DECLS

#define G3K_TYPE_KEYBOARD_BUTTON g3k_keyboard_button_get_type ()
G_DECLARE_FINAL_TYPE (G3kKeyboardButton,
                      g3k_keyboard_button,
                      G3K,
                      KEYBOARD_BUTTON,
                      G3kButton)

G3kKeyboardButton *
g3k_keyboard_button_new (G3kContext *g3k, graphene_size_t *size_meters);

G_END_DECLS

#endif /* G3K_KEYBOARD_BUTTON_H_ */
