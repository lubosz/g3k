/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Copyright 2010-2014 The three.js authors
 * Copyright 2014-2020 The gthree authors
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_LOADER_H_
#define G3K_LOADER_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>

#include "g3k-context.h"

G_BEGIN_DECLS

#define G3K_TYPE_LOADER g3k_loader_get_type ()
G_DECLARE_FINAL_TYPE (G3kLoader, g3k_loader, G3K, LOADER, GObject)

typedef struct _G3kLoader
{
  GObject parent;
} G3kLoader;

typedef enum
{
  G3K_LOADER_ERROR_FAIL,
} G3kLoaderError;

#define G3K_LOADER_ERROR (g3k_loader_error_quark ())

GQuark
g3k_loader_error_quark (void);

G3kLoader *
g3k_loader_new_from_file (G3kContext *context,
                          const char *file_path,
                          GError    **error);

G3kObject *
g3k_loader_get_root (G3kLoader *self);

G_END_DECLS

#endif /* G3K_LOADER_H_ */
