/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-material.h"

#include <stdalign.h>

typedef struct
{
  alignas (16) float diffuse_color[4];
  alignas (16) float emissive_color[4];
  alignas (8) float normal_scale[2];
  alignas (4) float ao_map_intensity;
  alignas (4) float emissive_intensity;
  alignas (4) float color_alpha;
  alignas (4) float roughness;
  alignas (4) float metalness;
} G3kMaterialUniformBuffer;

G_DEFINE_TYPE (G3kMaterial, g3k_material, G_TYPE_OBJECT)

static gboolean
_init (G3kMaterial *self)
{
  GulkanContext *gulkan = g3k_context_get_gulkan (self->context);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);
  self->buffer = gulkan_uniform_buffer_new (device,
                                            sizeof (G3kMaterialUniformBuffer));
  if (!self->buffer)
    {
      g_printerr ("Coult not init material buffer.\n");
      g_object_unref (self);
      return FALSE;
    }

  return TRUE;
}

G3kMaterial *
g3k_material_new (G3kContext *context)
{
  G3kMaterial *self = (G3kMaterial *) g_object_new (G3K_TYPE_MATERIAL, 0);
  self->context = g_object_ref (context);
  _init (self);
  return self;
}

G3kMaterial *
g3k_material_new_default (G3kContext *context)
{
  G3kMaterial *material = g3k_material_new (context);
  material->name = g_strdup ("default");
  return material;
}

static void
g3k_material_init (G3kMaterial *self)
{
  self->context = NULL;
  self->name = NULL;

  self->textures = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                          g_object_unref);
}

static void
_finalize (GObject *gobject)
{
  G3kMaterial *self = G3K_MATERIAL (gobject);
  if (self->name)
    {
      g_free (self->name);
    }

  g_hash_table_destroy (self->textures);

  g_clear_object (&self->buffer);
  g_clear_object (&self->context);

  G_OBJECT_CLASS (g3k_material_parent_class)->finalize (gobject);
}

static void
g3k_material_class_init (G3kMaterialClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

void
g3k_material_update_ubo (G3kMaterial *self)
{
  G3kMaterialUniformBuffer ub = {0};

  graphene_vec3_to_float (&self->color, ub.diffuse_color);
  graphene_vec3_to_float (&self->emissive, ub.emissive_color);
  graphene_vec2_to_float (&self->normal_scale, ub.normal_scale);

  ub.ao_map_intensity = self->ao_map_intensity;
  ub.emissive_intensity = self->emissive_intensity;
  ub.color_alpha = self->color_alpha;
  ub.roughness = self->roughness;
  ub.metalness = self->metalness;

  gulkan_uniform_buffer_update (self->buffer, (gpointer) &ub);
}

static const gchar *supported_textures[] = {
  "DIFFUSE", "NORMAL", "METALLIC_ROUGHNESS", "EMISSIVE", "AO",
};

void
g3k_material_add_texture (G3kMaterial   *self,
                          const char    *name,
                          GulkanTexture *texture)
{
  g_hash_table_insert (self->textures, g_strdup (name), g_object_ref (texture));
}

void
g3k_material_update_descriptors (G3kMaterial         *self,
                                 GulkanDescriptorSet *descriptor_set)
{
  gulkan_descriptor_set_update_buffer (descriptor_set, 1, self->buffer);

  guint j = 2;
  for (guint i = 0; i < G_N_ELEMENTS (supported_textures); i++)
    {
      if (g_hash_table_contains (self->textures, supported_textures[i]))
        {
          GulkanTexture *texture = g_hash_table_lookup (self->textures,
                                                        supported_textures[i]);
          gulkan_descriptor_set_update_texture_at (descriptor_set, j, i + 2,
                                                   texture);
          j++;
        }
    }
}

char *
g3k_material_get_defines (G3kMaterial *self)
{
  GString *s = g_string_new ("");

  for (guint i = 0; i < G_N_ELEMENTS (supported_textures); i++)
    {
      if (g_hash_table_contains (self->textures, supported_textures[i]))
        {
          g_string_append_printf (s, "#define HAVE_%s_MAP\n",
                                  supported_textures[i]);
        }
    }
  return g_string_free (s, FALSE);
}

char *
g3k_material_get_config_key (G3kMaterial *self)
{
  GString *s = g_string_new ("");
  for (guint i = 0; i < G_N_ELEMENTS (supported_textures); i++)
    {
      if (g_hash_table_contains (self->textures, supported_textures[i]))
        {
          g_string_append (s, supported_textures[i]);
        }
    }
  return g_string_free (s, FALSE);
}

VkDescriptorSetLayoutBinding *
g3k_material_create_descriptor_bindings (G3kMaterial *self, uint32_t *size)
{
  *size = 2 + g_hash_table_size (self->textures);

  VkDescriptorSetLayoutBinding *bindings
    = g_malloc (sizeof (VkDescriptorSetLayoutBinding) * *size);

  // transformation buffer
  bindings[0] = (VkDescriptorSetLayoutBinding){
    .binding = 0,
    .descriptorCount = 1,
    .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
  };

  // material buffer
  bindings[1] = (VkDescriptorSetLayoutBinding){
    .binding = 1,
    .descriptorCount = 1,
    .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
  };

  // texture bindings
  guint j = 2;
  for (guint i = 0; i < G_N_ELEMENTS (supported_textures); i++)
    {
      if (g_hash_table_contains (self->textures, supported_textures[i]))
        {
          bindings[j] = (VkDescriptorSetLayoutBinding){
            .binding = i + 2,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
          };
          j++;
        }
    }
  return bindings;
}
