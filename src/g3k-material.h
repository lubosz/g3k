/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_MATERIAL_H_
#define G3K_MATERIAL_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#include "g3k-context.h"

struct _G3kMaterial
{
  GObject parent;

  G3kContext *context;

  char *name;

  float ao_map_intensity;

  graphene_vec2_t normal_scale;

  graphene_vec3_t emissive;
  float           emissive_intensity;

  graphene_vec3_t color;
  float           color_alpha;
  float           roughness;
  float           metalness;

  float alpha_test;

  gboolean double_sided;
  gboolean transparent;

  GHashTable *textures;

  GulkanUniformBuffer *buffer;
};

#define G3K_TYPE_MATERIAL g3k_material_get_type ()
G_DECLARE_FINAL_TYPE (G3kMaterial, g3k_material, G3K, MATERIAL, GObject)

G3kMaterial *
g3k_material_new (G3kContext *context);

G3kMaterial *
g3k_material_new_default (G3kContext *context);

void
g3k_material_update_ubo (G3kMaterial *self);

void
g3k_material_update_descriptors (G3kMaterial         *self,
                                 GulkanDescriptorSet *descriptor_set);

char *
g3k_material_get_defines (G3kMaterial *self);

VkDescriptorSetLayoutBinding *
g3k_material_create_descriptor_bindings (G3kMaterial *self, uint32_t *size);

char *
g3k_material_get_config_key (G3kMaterial *self);

void
g3k_material_add_texture (G3kMaterial   *self,
                          const char    *name,
                          GulkanTexture *texture);

G_END_DECLS

#endif /* G3K_MATERIAL_H_ */
