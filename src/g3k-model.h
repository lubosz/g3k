/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_MODEL_H_
#define G3K_MODEL_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-mesh.h"
#include "g3k-object.h"

G_BEGIN_DECLS

#define G3K_TYPE_MODEL g3k_model_get_type ()
G_DECLARE_FINAL_TYPE (G3kModel, g3k_model, G3K, MODEL, G3kObject)

G3kModel *
g3k_model_new (G3kContext *g3k);

void
g3k_model_set_mesh (G3kModel *self, G3kMesh *mesh);

void
g3k_model_set_name (G3kModel *self, char *name);

char *
g3k_model_get_name (G3kModel *self);

bool
g3k_model_initialize (G3kModel *self, GHashTable *pipelines);

G_END_DECLS

#endif /* G3K_MODEL_H_ */
