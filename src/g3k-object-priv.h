/*
 * xrdesktop
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_OBJECT_PRIV_H_
#define G3K_OBJECT_PRIV_H_

#include "g3k-object.h"
#include "g3k-types.h"

void
g3k_object_emit_grab (G3kObject *self, G3kGrabEvent *event);

void
g3k_object_emit_release (G3kObject *self, G3kController *controller);

void
g3k_object_emit_hover_end (G3kObject *self, G3kController *controller);

void
g3k_object_emit_hover (G3kObject *self, G3kHoverEvent *event);

void
g3k_object_emit_hover_start (G3kObject *self, G3kController *controller);

void
g3k_object_set_context (G3kObject *self, G3kContext *context);

gboolean
g3k_object_initialize_no_pipeline (G3kObject   *self,
                                   G3kContext  *g3k,
                                   VkDeviceSize uniform_buffer_size);

#endif /* G3K_OBJECT_PRIV_H_ */
