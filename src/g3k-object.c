/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-object-priv.h"

#include <gdk/gdk.h>
#include <graphene-ext.h>

#include "g3k-selection.h"

typedef struct _G3kObjectPrivate
{
  GObject object;

  G3kContext          *g3k;
  GulkanUniformBuffer *transformation_ubo;
  GulkanDescriptorSet *descriptor_set;
  gboolean             visible;
  gboolean             initialized;
  G3kPipeline         *pipeline;

  /* Scene graph */
  G3kObject *parent;
  GSList    *children;

  /*
   * Local / Relative pose and scale. Global / Absolute pose and scale
   * will be calculated from this and the parent node.
   */
  G3kPose            pose;
  graphene_point3d_t scale;

  /*
   * A higher drawing_order means the object will be drawn after a sibling of
   * lower order. Only evaluated in relation to siblings. Default is 0.
   */
  uint32_t drawing_order;
} G3kObjectPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (G3kObject, g3k_object, G_TYPE_OBJECT)

enum
{
  GRAB_START_EVENT,
  GRAB_EVENT,
  RELEASE_EVENT,
  HOVER_START_EVENT,
  HOVER_EVENT,
  HOVER_END_EVENT,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = {0};

static void
g3k_object_finalize (GObject *gobject);

static void
_set_local_pose_from_global (G3kObject         *self,
                             graphene_matrix_t *m,
                             gboolean           set_scale);

static void
g3k_object_class_init (G3kObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = g3k_object_finalize;

  signals[GRAB_START_EVENT] = g_signal_new ("grab-start-event",
                                            G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            NULL, G_TYPE_NONE, 1,
                                            GDK_TYPE_EVENT
                                              | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[GRAB_EVENT] = g_signal_new ("grab-event", G_TYPE_FROM_CLASS (klass),
                                      G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL,
                                      G_TYPE_NONE, 1,
                                      GDK_TYPE_EVENT
                                        | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[RELEASE_EVENT] = g_signal_new ("release-event",
                                         G_TYPE_FROM_CLASS (klass),
                                         G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                         NULL, G_TYPE_NONE, 1,
                                         GDK_TYPE_EVENT
                                           | G_SIGNAL_TYPE_STATIC_SCOPE);
  signals[HOVER_END_EVENT] = g_signal_new ("hover-end-event",
                                           G_TYPE_FROM_CLASS (klass),
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                           NULL, G_TYPE_NONE, 1,
                                           GDK_TYPE_EVENT
                                             | G_SIGNAL_TYPE_STATIC_SCOPE);
  signals[HOVER_EVENT] = g_signal_new ("hover-event", G_TYPE_FROM_CLASS (klass),
                                       G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                       G_TYPE_NONE, 1,
                                       GDK_TYPE_EVENT
                                         | G_SIGNAL_TYPE_STATIC_SCOPE);
  signals[HOVER_START_EVENT] = g_signal_new ("hover-start-event",
                                             G_TYPE_FROM_CLASS (klass),
                                             G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                             NULL, G_TYPE_NONE, 1,
                                             GDK_TYPE_EVENT
                                               | G_SIGNAL_TYPE_STATIC_SCOPE);
}

static void
g3k_object_init (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  priv->descriptor_set = NULL;
  priv->visible = TRUE;
  priv->initialized = FALSE;
  priv->g3k = NULL;
  priv->parent = NULL;
  priv->children = NULL;
  priv->transformation_ubo = NULL;

  priv->pose = g3k_pose_new (NULL, NULL);
  graphene_point3d_init (&priv->scale, 1.0, 1.0, 1.0);

  priv->drawing_order = 0;
}

G3kObject *
g3k_object_new ()
{
  G3kObject *self = (G3kObject *) g_object_new (G3K_TYPE_OBJECT, 0);
  return self;
}

static void
g3k_object_finalize (GObject *gobject)
{
  G3kObject        *self = G3K_OBJECT (gobject);
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  for (GSList *l = priv->children; l; l = l->next)
    {
      G3kObject        *child = G3K_OBJECT (l->data);
      G3kObjectPrivate *child_priv = g3k_object_get_instance_private (child);
      // TODO: notify children that parent has been destroyed
      child_priv->parent = NULL;
    }
  if (priv->parent)
    {
      g3k_object_remove_child (priv->parent, self);
    }

  if (!priv->initialized)
    return;

  g_clear_object (&priv->descriptor_set);
  g_clear_object (&priv->transformation_ubo);
  g_clear_object (&priv->g3k);

  G_OBJECT_CLASS (g3k_object_parent_class)->finalize (gobject);
}

#ifdef VALIDATE_TRANSFORM
static void
_validate_point_distance (graphene_point3d_t *p)
{
  if (graphene_point3d_length (p) > 5000)
    {
      g_warning ("Point %f %f %f\n", (double) p->x, (double) p->y,
                 (double) p->z);
      g_assert ("Very far away" && false);
    }
}
#endif

void
g3k_object_bind (G3kObject *self, VkCommandBuffer cmd_buffer)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  g3k_pipeline_bind (priv->pipeline, cmd_buffer);

  VkPipelineLayout layout = g3k_pipeline_get_layout (priv->pipeline);
  gulkan_descriptor_set_bind (priv->descriptor_set, layout, cmd_buffer);
}

void
g3k_object_set_scale (G3kObject *self, graphene_point3d_t *scale)
{
#ifdef VALIDATE_TRANSFORM
  graphene_ext_point3d_validate_all_nonzero (scale);
#endif
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  graphene_point3d_t current_global_scale;
  g3k_object_get_scale (self, &current_global_scale);

  /* What factor is the difference of the new global scale to the current global
   * scale */
  graphene_point3d_t factor = {
    .x = scale->x / current_global_scale.x,
    .y = scale->y / current_global_scale.y,
    .z = scale->z / current_global_scale.z,
  };

  priv->scale.x *= factor.x;
  priv->scale.y *= factor.y;
  priv->scale.z *= factor.z;

#ifdef VALIDATE_TRANSFORM
  graphene_ext_point3d_validate_all_nonzero (&priv->scale);
#endif
}

void
g3k_object_get_scale (G3kObject *self, graphene_point3d_t *out_scale)
{
  graphene_matrix_t m;
  g3k_object_get_matrix (self, &m);
  graphene_ext_matrix_get_scale (&m, out_scale);

#ifdef VALIDATE_TRANSFORM
  graphene_ext_point3d_validate_all_nonzero (out_scale);
#endif
}

G3kPose
g3k_object_get_pose (G3kObject *self)
{
  graphene_matrix_t m;
  g3k_object_get_matrix (self, &m);
  return g3k_pose_new_from_matrix (&m);
}

void
g3k_object_set_pose (G3kObject *self, G3kPose *pose)
{
  graphene_matrix_t g;
  g3k_pose_to_matrix (pose, &g);
  // TODO; Decompose less
  _set_local_pose_from_global (self, &g, false);
}

void
g3k_object_get_position (G3kObject *self, graphene_point3d_t *position)
{
  graphene_matrix_t m;
  g3k_object_get_matrix (self, &m);

  graphene_vec3_t position_v;
  graphene_ext_matrix_get_translation_vec3 (&m, &position_v);

  graphene_point3d_init_from_vec3 (position, &position_v);
}

void
g3k_object_set_context (G3kObject *self, G3kContext *g3k)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  priv->g3k = g_object_ref (g3k);
}

gboolean
g3k_object_initialize (G3kObject   *self,
                       G3kContext  *g3k,
                       G3kPipeline *pipeline,
                       VkDeviceSize uniform_buffer_size)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  g3k_object_set_context (self, g3k);
  priv->pipeline = g_object_ref (pipeline);

  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);

  GulkanDevice *device = gulkan_context_get_device (gulkan);

  /* Create uniform buffer to hold a matrix per eye */
  priv->transformation_ubo = gulkan_uniform_buffer_new (device,
                                                        uniform_buffer_size);
  if (!priv->transformation_ubo)
    return FALSE;

  priv->descriptor_set = g3k_pipeline_create_descriptor_set (priv->pipeline);
  g_assert (priv->descriptor_set);
  gulkan_descriptor_set_update_buffer (priv->descriptor_set, 0,
                                       priv->transformation_ubo);

  priv->initialized = TRUE;

  return TRUE;
}

gboolean
g3k_object_initialize_no_pipeline (G3kObject   *self,
                                   G3kContext  *g3k,
                                   VkDeviceSize uniform_buffer_size)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  g3k_object_set_context (self, g3k);

  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);

  GulkanDevice *device = gulkan_context_get_device (gulkan);

  /* Create uniform buffer to hold a matrix per eye */
  priv->transformation_ubo = gulkan_uniform_buffer_new (device,
                                                        uniform_buffer_size);
  if (!priv->transformation_ubo)
    return FALSE;

  priv->initialized = TRUE;

  return TRUE;
}

#ifdef VALIDATE_TRANSFORM
static void
_print_decomposed (graphene_matrix_t *m)
{
  graphene_point3d_t    pos;
  graphene_quaternion_t rot;
  graphene_point3d_t    scale;
  graphene_ext_matrix_decompose (m, &scale, &rot, &pos);

  g_print ("pos   | %f %f %f |\n", (double) pos.x, (double) pos.y,
           (double) pos.z);
  g_print ("rot   ");
  graphene_ext_quaternion_print (&rot);
  g_print ("scale | %f %f %f |\n", (double) scale.x, (double) scale.y,
           (double) scale.z);
}

static bool
_validate_decomposed_matrices (graphene_matrix_t *expected,
                               graphene_matrix_t *got,
                               float              epsilon,
                               bool               check_scale)
{
  graphene_point3d_t    scale_e;
  graphene_quaternion_t rot_e;
  graphene_point3d_t    pos_e;
  graphene_ext_matrix_decompose (expected, &scale_e, &rot_e, &pos_e);

  graphene_point3d_t    scale_g;
  graphene_quaternion_t rot_g;
  graphene_point3d_t    pos_g;
  graphene_ext_matrix_decompose (got, &scale_g, &rot_g, &pos_g);

  if (!graphene_ext_point3d_near (&pos_e, &pos_g, epsilon))
    {
      return false;
    }

  if (!graphene_ext_quaternion_near (&rot_e, &rot_g, epsilon))
    {
      return false;
    }

  if (check_scale && !graphene_ext_point3d_near (&scale_e, &scale_g, epsilon))
    {
      return false;
    }

  return true;
}

#define THRESHOLD 0.1f

static gboolean
_check_matrix_diff (graphene_matrix_t *expected,
                    graphene_matrix_t *got,
                    bool               check_scale,
                    const char        *type)
{
  if (_validate_decomposed_matrices (expected, got, THRESHOLD, check_scale))
    return TRUE;

  g_printerr ("Failed to validate %s transformation with threshold of %f"
              " with scale: %d.\n",
              type, (double) THRESHOLD, check_scale);
  g_printerr ("Expected:\n");
  graphene_matrix_print (expected);
  _print_decomposed (expected);
  g_printerr ("Got:\n");
  graphene_matrix_print (got);
  _print_decomposed (got);

  return FALSE;
}
#endif

/*
 * Calculate difference between current global pose and new global pose.
 * Then add that difference to the current local pose.
 */
static void
_set_local_pose_from_global (G3kObject         *self,
                             graphene_matrix_t *m,
                             gboolean           set_scale)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  G3kObject        *parent = priv->parent;

  graphene_matrix_t local_transformation;
  if (parent)
    {
      graphene_matrix_t parent_transformation;
      g3k_object_get_matrix (parent, &parent_transformation);

      graphene_matrix_t parent_transformation_inv;
      g_assert (graphene_matrix_inverse (&parent_transformation,
                                         &parent_transformation_inv));

      graphene_matrix_multiply (m, &parent_transformation_inv,
                                &local_transformation);

#ifdef VALIDATE_TRANSFORM
      graphene_matrix_t check_global_transform;
      graphene_matrix_multiply (&local_transformation, &parent_transformation,
                                &check_global_transform);
      g_assert (graphene_matrix_near (&check_global_transform, m, 0.01f));
#endif
    }
  else
    {
      graphene_matrix_init_from_matrix (&local_transformation, m);
    }

  graphene_point3d_t    pos;
  graphene_quaternion_t rot;
  graphene_point3d_t    scale;

  graphene_ext_matrix_decompose (&local_transformation, &scale, &rot, &pos);

  priv->pose = g3k_pose_new (&pos, &rot);

  if (set_scale)
    {
      graphene_point3d_init_from_point (&priv->scale, &scale);
    }

#ifdef VALIDATE_TRANSFORM
  _validate_point_distance (&pos);

  graphene_ext_point3d_validate (&pos);
  graphene_ext_quaternion_validate (&rot);
  graphene_ext_point3d_validate_all_nonzero (&scale);
  if (set_scale)
    {
      graphene_matrix_t g;
      graphene_matrix_t l;
      g3k_object_get_matrix (self, &g);
      g3k_object_get_local_matrix (self, &l);
      g_assert (_check_matrix_diff (m, &g, set_scale, "global"));
      g_assert (_check_matrix_diff (&local_transformation, &l, set_scale,
                                    "local"));
    }
#endif
}

/*
 * Set transformation without matrix decomposition and ability to rebuild
 * This will include scale as well.
 */
void
g3k_object_set_matrix (G3kObject *self, graphene_matrix_t *m)
{
  _set_local_pose_from_global (self, m, true);

#ifdef VALIDATE_TRANSFORM
  graphene_point3d_t p;
  graphene_ext_matrix_get_translation_point3d (m, &p);
  _validate_point_distance (&p);
#endif
}

void
g3k_object_get_local_matrix (G3kObject *self, graphene_matrix_t *m)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  graphene_matrix_init_scale (m, priv->scale.x, priv->scale.y, priv->scale.z);
  graphene_matrix_rotate_quaternion (m, &priv->pose.rotation);
  graphene_matrix_translate (m, &priv->pose.position);

#ifdef VALIDATE_TRANSFORM
  graphene_ext_matrix_validate (m);
#endif
}

void
g3k_object_get_matrix (G3kObject *self, graphene_matrix_t *m)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  G3kObject        *parent = priv->parent;

  if (parent)
    {
      graphene_matrix_t parent_transformation;
      g3k_object_get_matrix (parent, &parent_transformation);

      graphene_matrix_t local_transformation;
      g3k_object_get_local_matrix (self, &local_transformation);

      graphene_matrix_multiply (&local_transformation, &parent_transformation,
                                m);
    }
  else
    {
      g3k_object_get_local_matrix (self, m);
    }

#ifdef VALIDATE_TRANSFORM
  graphene_ext_matrix_validate (m);
#endif
}

void
g3k_object_set_local_matrix (G3kObject *self, graphene_matrix_t *m)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  graphene_point3d_t    pos;
  graphene_quaternion_t rot;
  graphene_point3d_t    scale;

  graphene_ext_matrix_decompose (m, &scale, &rot, &pos);
  priv->pose = g3k_pose_new (&pos, &rot);
  graphene_point3d_init_from_point (&priv->scale, &scale);

#ifdef VALIDATE_TRANSFORM
  graphene_ext_point3d_validate (&pos);
  graphene_ext_quaternion_validate (&rot);
  graphene_ext_point3d_validate_all_nonzero (&scale);
#endif
}

void
g3k_object_set_local_scale (G3kObject *self, graphene_point3d_t *scale)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  graphene_point3d_init_from_point (&priv->scale, scale);

#ifdef VALIDATE_TRANSFORM
  graphene_ext_point3d_validate_all_nonzero (scale);
#endif
}

void
g3k_object_get_local_scale (G3kObject *self, graphene_point3d_t *scale)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  graphene_point3d_init_from_point (scale, &priv->scale);

#ifdef VALIDATE_TRANSFORM
  graphene_ext_point3d_validate_all_nonzero (scale);
#endif
}

void
g3k_object_set_local_pose (G3kObject *self, G3kPose *pose)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  g3k_pose_init_from_pose (&priv->pose, pose);

#ifdef VALIDATE_TRANSFORM
  _validate_point_distance (&priv->pose.position);
#endif
}

gboolean
g3k_object_is_visible (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  return priv->visible;
}

void
g3k_object_set_visibility (G3kObject *self, gboolean is_visible)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  priv->visible = is_visible;
}

GulkanUniformBuffer *
g3k_object_get_transformation_ubo (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  return priv->transformation_ubo;
}

GulkanDescriptorSet *
g3k_object_get_descriptor_set (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  return priv->descriptor_set;
}

void
g3k_object_update_transformation_ubo (G3kObject *self, gpointer data)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  gulkan_uniform_buffer_update (priv->transformation_ubo, data);
}

G3kContext *
g3k_object_get_context (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  return priv->g3k;
}

void
g3k_object_draw (G3kObject *self, VkCommandBuffer cmd_buffer)
{
  G3kObjectClass   *klass = G3K_OBJECT_GET_CLASS (self);
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  if (g3k_object_is_visible (self))
    {
      if (klass->draw != NULL && priv->initialized)
        klass->draw (self, cmd_buffer);
    }

  for (GSList *l = priv->children; l; l = l->next)
    {
      G3kObject *child = G3K_OBJECT (l->data);
      g3k_object_draw (child, cmd_buffer);
    }
}

G3kPipeline *
g3k_object_get_pipeline (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  return priv->pipeline;
}

gboolean
g3k_object_get_intersection (G3kObject          *self,
                             graphene_ray_t     *ray,
                             float              *distance,
                             graphene_point3d_t *res)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  G3kObjectClass *klass = G3K_OBJECT_GET_CLASS (self);
  if (klass->get_intersection != NULL && priv->initialized)
    return klass->get_intersection (self, ray, distance, res);

  g_printerr ("Intersecting uninitialized object");
  return FALSE;
}

gboolean
g3k_object_update_selection (G3kObject *self, G3kSelection *selection)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);

  G3kObjectClass *klass = G3K_OBJECT_GET_CLASS (self);
  if (klass->update_selection != NULL && priv->initialized)
    return klass->update_selection (self, selection);

  g_printerr ("Updating selection on uninitialized object");
  return FALSE;
}

G3kObject *
g3k_object_get_parent (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  return priv->parent;
}

GSList *
g3k_object_get_children (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  return priv->children;
}

static gint
_sort_drawing_order (gconstpointer a_, gconstpointer b_)
{
  G3kObjectPrivate *a = g3k_object_get_instance_private (G3K_OBJECT ((gpointer)
                                                                       a_));
  G3kObjectPrivate *b = g3k_object_get_instance_private (G3K_OBJECT ((gpointer)
                                                                       b_));

  if (a->drawing_order < b->drawing_order)
    {
      return -1;
    }
  else if (a->drawing_order == b->drawing_order)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

void
g3k_object_add_child (G3kObject *self,
                      G3kObject *child,
                      uint32_t   child_drawing_order)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  G3kObjectPrivate *child_priv = g3k_object_get_instance_private (child);

  priv->children = g_slist_append (priv->children, child);
  child_priv->parent = self;
  child_priv->drawing_order = child_drawing_order;

  priv->children = g_slist_sort (priv->children, _sort_drawing_order);
}

void
g3k_object_remove_child (G3kObject *self, G3kObject *child)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  G3kObjectPrivate *child_priv = g3k_object_get_instance_private (child);

  child_priv->parent = NULL;
  priv->children = g_slist_remove (priv->children, child);
}

/**
 * xrd_window_get_object_space_intersection:
 * @self: The #XrdWindow
 * @intersection_3d: A #graphene_point3d_t intersection point in meters.
 * @intersection_2d: Intersection in object coordinates with origin at center in
 * meters.
 */
void
g3k_object_get_object_space_intersection (G3kObject          *self,
                                          graphene_point3d_t *intersection_3d,
                                          graphene_point_t   *intersection_2d)
{
  graphene_matrix_t transform;

  G3kPose pose = g3k_object_get_pose (self);
  g3k_pose_to_matrix (&pose, &transform);

  graphene_matrix_t inverse_transform;
  graphene_matrix_inverse (&transform, &inverse_transform);

  graphene_point3d_t intersection_origin;
  graphene_matrix_transform_point3d (&inverse_transform, intersection_3d,
                                     &intersection_origin);

  graphene_point_init (intersection_2d, intersection_origin.x,
                       intersection_origin.y);
}

void
g3k_object_reset_local_scale (G3kObject *self)
{
  G3kObjectPrivate *priv = g3k_object_get_instance_private (self);
  graphene_point3d_init (&priv->scale, 1.0f, 1.0f, 1.0f);
}

void
g3k_object_emit_grab_start (G3kObject *self, G3kController *controller)
{
  g_signal_emit (self, signals[GRAB_START_EVENT], 0, controller);
}

void
g3k_object_emit_grab (G3kObject *self, G3kGrabEvent *event)
{
  g_signal_emit (self, signals[GRAB_EVENT], 0, event);
}

void
g3k_object_emit_release (G3kObject *self, G3kController *controller)
{
  g_signal_emit (self, signals[RELEASE_EVENT], 0, controller);
}

void
g3k_object_emit_hover_end (G3kObject *self, G3kController *controller)
{
  g_signal_emit (self, signals[HOVER_END_EVENT], 0, controller);
}

void
g3k_object_emit_hover (G3kObject *self, G3kHoverEvent *event)
{
  g_signal_emit (self, signals[HOVER_EVENT], 0, event);
}

void
g3k_object_emit_hover_start (G3kObject *self, G3kController *controller)
{
  g_signal_emit (self, signals[HOVER_START_EVENT], 0, controller);
}
