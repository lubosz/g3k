/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_OBJECT_H_
#define G3K_OBJECT_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-context.h"
#include "g3k-pipeline.h"
#include "g3k-pose.h"

G_BEGIN_DECLS

typedef struct _G3kSelection G3kSelection;
#define G3K_TYPE_OBJECT g3k_object_get_type ()
G_DECLARE_DERIVABLE_TYPE (G3kObject, g3k_object, G3K, OBJECT, GObject)

/**
 * G3kObjectClass:
 * @parent: The object class structure needs to be the first
 *   element in the widget class structure in order for the class mechanism
 *   to work correctly. This allows a G3kObjectClass pointer to be cast to
 *   a GObjectClass pointer.
 * @update_selection: Implement this function to appropriately shape a
 *   @G3kSelection around this object.
 */
struct _G3kObjectClass
{
  GObjectClass parent;

  void (*draw) (G3kObject *self, VkCommandBuffer cmd_buffer);

  gboolean (*get_intersection) (G3kObject          *self,
                                graphene_ray_t     *ray,
                                float              *distance,
                                graphene_point3d_t *res);

  gboolean (*update_selection) (G3kObject *self, G3kSelection *selection);
};

G3kObject *
g3k_object_new (void);

void
g3k_object_bind (G3kObject *self, VkCommandBuffer cmd_buffer);

gboolean
g3k_object_initialize (G3kObject   *self,
                       G3kContext  *g3k,
                       G3kPipeline *pipeline,
                       VkDeviceSize uniform_buffer_size);

void
g3k_object_set_scale (G3kObject *self, graphene_point3d_t *scale);

void
g3k_object_get_scale (G3kObject *self, graphene_point3d_t *out_scale);

void
g3k_object_get_position (G3kObject *self, graphene_point3d_t *position);

void
g3k_object_get_matrix (G3kObject *self, graphene_matrix_t *m);

void
g3k_object_get_local_matrix (G3kObject *self, graphene_matrix_t *m);

void
g3k_object_set_local_matrix (G3kObject *self, graphene_matrix_t *m);

void
g3k_object_set_local_scale (G3kObject *self, graphene_point3d_t *scale);

void
g3k_object_get_local_scale (G3kObject *self, graphene_point3d_t *scale);

void
g3k_object_set_matrix (G3kObject *self, graphene_matrix_t *m);

void
g3k_object_set_local_pose (G3kObject *self, G3kPose *pose);

G3kPose
g3k_object_get_pose (G3kObject *self);

gboolean
g3k_object_is_visible (G3kObject *self);

void
g3k_object_set_visibility (G3kObject *self, gboolean is_visible);

GulkanUniformBuffer *
g3k_object_get_transformation_ubo (G3kObject *self);

void
g3k_object_update_transformation_ubo (G3kObject *self, gpointer data);

GulkanDescriptorSet *
g3k_object_get_descriptor_set (G3kObject *self);

G3kContext *
g3k_object_get_context (G3kObject *self);

void
g3k_object_draw (G3kObject *self, VkCommandBuffer cmd_buffer);

G3kPipeline *
g3k_object_get_pipeline (G3kObject *self);

gboolean
g3k_object_get_intersection (G3kObject          *self,
                             graphene_ray_t     *ray,
                             float              *distance,
                             graphene_point3d_t *res);

gboolean
g3k_object_update_selection (G3kObject *self, G3kSelection *selection);

G3kObject *
g3k_object_get_parent (G3kObject *self);

GSList *
g3k_object_get_children (G3kObject *self);

void
g3k_object_add_child (G3kObject *self,
                      G3kObject *child,
                      uint32_t   child_drawing_order);

void
g3k_object_remove_child (G3kObject *self, G3kObject *child);

void
g3k_object_get_object_space_intersection (G3kObject          *self,
                                          graphene_point3d_t *intersection_3d,
                                          graphene_point_t   *intersection_2d);

void
g3k_object_reset_local_scale (G3kObject *self);

void
g3k_object_set_pose (G3kObject *self, G3kPose *pose);

void
g3k_object_emit_grab_start (G3kObject *self, G3kController *controller);

G_END_DECLS

#endif /* G3K_OBJECT_H_ */
