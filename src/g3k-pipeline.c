/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-pipeline.h"

struct _G3kPipeline
{
  GObject parent;

  G3kContext *context;

  GulkanPipeline       *pipeline;
  GulkanDescriptorPool *descriptor_pool;
};

G_DEFINE_TYPE (G3kPipeline, g3k_pipeline, G_TYPE_OBJECT)

static void
g3k_pipeline_init (G3kPipeline *self)
{
  (void) self;
}

G3kPipeline *
g3k_pipeline_new (G3kContext                         *context,
                  const VkDescriptorSetLayoutBinding *bindings,
                  uint32_t                            set_size,
                  uint32_t                            max_sets,
                  GulkanPipelineConfig               *config,
                  GulkanRenderPass                   *render_pass)
{
  G3kPipeline *self = (G3kPipeline *) g_object_new (G3K_TYPE_PIPELINE, 0);
  self->context = g_object_ref (context);

  GulkanContext *gulkan = g3k_context_get_gulkan (self->context);
  self->descriptor_pool = gulkan_descriptor_pool_new (gulkan, bindings,
                                                      set_size, max_sets);

  self->pipeline = gulkan_pipeline_new (gulkan, self->descriptor_pool,
                                        render_pass, config);

  return self;
}

static void
_finalize (GObject *gobject)
{
  G3kPipeline *self = G3K_PIPELINE (gobject);
  g_object_unref (self->context);
  g_object_unref (self->descriptor_pool);
  g_object_unref (self->pipeline);
  G_OBJECT_CLASS (g3k_pipeline_parent_class)->finalize (gobject);
}

static void
g3k_pipeline_class_init (G3kPipelineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

void
g3k_pipeline_bind (G3kPipeline *self, VkCommandBuffer cmd_buffer)
{
  gulkan_pipeline_bind (self->pipeline, cmd_buffer);
}

VkPipelineLayout
g3k_pipeline_get_layout (G3kPipeline *self)
{
  return gulkan_descriptor_pool_get_pipeline_layout (self->descriptor_pool);
}

GulkanDescriptorSet *
g3k_pipeline_create_descriptor_set (G3kPipeline *self)
{
  return gulkan_descriptor_pool_create_set (self->descriptor_pool);
}
