/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_PIPELINE_H_
#define G3K_PIPELINE_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-context.h"

G_BEGIN_DECLS

#define G3K_TYPE_PIPELINE g3k_pipeline_get_type ()
G_DECLARE_FINAL_TYPE (G3kPipeline, g3k_pipeline, G3K, PIPELINE, GObject)

G3kPipeline *
g3k_pipeline_new (G3kContext                         *context,
                  const VkDescriptorSetLayoutBinding *bindings,
                  uint32_t                            set_size,
                  uint32_t                            max_sets,
                  GulkanPipelineConfig               *config,
                  GulkanRenderPass                   *render_pass);

void
g3k_pipeline_bind (G3kPipeline *self, VkCommandBuffer cmd_buffer);

VkPipelineLayout
g3k_pipeline_get_layout (G3kPipeline *self);

GulkanDescriptorSet *
g3k_pipeline_create_descriptor_set (G3kPipeline *self);

G_END_DECLS

#endif /* G3K_PIPELINE_H_ */
