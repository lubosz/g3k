/*
 * g3k
 * Copyright 2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-plane-priv.h"

#include <graphene-ext.h>

#include "g3k-ppm.h"

typedef struct _G3kPlanePrivate
{
  G3kObject parent;

  GulkanVertexBuffer *vertex_buffer;
  GulkanTexture      *texture;

  graphene_size_t size_meters;
} G3kPlanePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (G3kPlane, g3k_plane, G3K_TYPE_OBJECT)

static void
g3k_plane_init (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  priv->texture = NULL;
  priv->vertex_buffer = NULL;
  graphene_size_init (&priv->size_meters, 1.0f, 1.0f);
}

G3kPlane *
g3k_plane_new (void)
{
  return (G3kPlane *) g_object_new (G3K_TYPE_PLANE, 0);
}

static void
_finalize (GObject *gobject)
{
  G3kPlane        *self = G3K_PLANE (gobject);
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);

  g_clear_object (&priv->vertex_buffer);
  g_clear_object (&priv->texture);

  G_OBJECT_CLASS (g3k_plane_parent_class)->finalize (gobject);
}

static void
_update_ubo (G3kPlane *self)
{
  G3kPlaneUniformBuffer ub = {0};

  graphene_matrix_t m_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &m_matrix);

  G3kContext        *context = g3k_object_get_context (G3K_OBJECT (self));
  graphene_matrix_t *vp = g3k_context_get_vps (context);

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_matrix_t mvp_matrix;
      graphene_matrix_multiply (&m_matrix, &vp[eye], &mvp_matrix);
      graphene_matrix_to_float (&mvp_matrix, ub.mvp[eye]);
    }

  g3k_object_update_transformation_ubo (G3K_OBJECT (self), &ub);
}

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer)
{
  G3kPlane        *self = G3K_PLANE (obj);
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);

  if (!g3k_plane_has_texture (self))
    {
      /* g_warning ("Trying to draw window with no texture.\n"); */
      return;
    }

  _update_ubo (self);

  g3k_object_bind (obj, cmd_buffer);
  gulkan_vertex_buffer_draw (priv->vertex_buffer, cmd_buffer);
}

static GulkanVertexBuffer *
_init_mesh (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  G3kContext      *g3k = g3k_object_get_context (G3K_OBJECT (self));
  GulkanContext   *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice    *device = gulkan_context_get_device (gulkan);

  GulkanVertexBuffer *vertex_buffer
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);

  graphene_matrix_t m;
  graphene_matrix_init_identity (&m);

  graphene_point_t from = {
    .x = -priv->size_meters.width / 2.0f,
    .y = -priv->size_meters.height / 2.0f,
  };
  graphene_point_t to = {
    .x = priv->size_meters.width / 2.0f,
    .y = priv->size_meters.height / 2.0f,
  };

  gulkan_geometry_append_plane (vertex_buffer, &from, &to, &m);

  if (!gulkan_vertex_buffer_alloc_array (vertex_buffer))
    {
      g_object_unref (vertex_buffer);
      return NULL;
    }

  return vertex_buffer;
}

static void
_update_texture_size (G3kPlane *self)
{
  g3k_plane_update_mesh (self);
}

static gboolean
_get_intersection (G3kObject          *self,
                   graphene_ray_t     *ray,
                   float              *distance,
                   graphene_point3d_t *res);

static VkExtent2D
_get_content_extent (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  return gulkan_texture_get_extent (priv->texture);
}

static void
g3k_plane_class_init (G3kPlaneClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;

  G3kObjectClass *g3k_object_class = G3K_OBJECT_CLASS (klass);
  g3k_object_class->draw = _draw;
  g3k_object_class->get_intersection = _get_intersection;

  klass->init_mesh = _init_mesh;
  klass->update_texture_size = _update_texture_size;
  klass->get_content_extent = _get_content_extent;
}

void
g3k_plane_set_mesh_size (G3kPlane *self, graphene_size_t *size_meters)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);

  if (!graphene_size_equal (&priv->size_meters, size_meters)
      || !priv->vertex_buffer)
    {
      graphene_size_init_from_size (&priv->size_meters, size_meters);
      g3k_plane_update_mesh (self);
    }
}

graphene_size_t
g3k_plane_get_mesh_size (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  return priv->size_meters;
}

void
g3k_plane_set_texture (G3kPlane *self, GulkanTexture *texture)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);

  if (texture == priv->texture)
    {
      g_debug ("Texture %p was already set %p.", (void *) texture,
               (void *) self);
      return;
    }

  VkExtent2D extent = gulkan_texture_get_extent (texture);

  gboolean got_new_size = TRUE;

  if (priv->texture)
    {
      VkExtent2D old_extent = gulkan_texture_get_extent (priv->texture);
      if (extent.width == old_extent.width
          && extent.height == old_extent.height)
        {
          got_new_size = FALSE;
        }
    }

  g_clear_object (&priv->texture);

  priv->texture = texture;

  gulkan_texture_init_sampler (priv->texture, VK_FILTER_LINEAR,
                               VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);

  GulkanDescriptorSet *descriptor_set
    = g3k_object_get_descriptor_set (G3K_OBJECT (self));

  gulkan_descriptor_set_update_texture (descriptor_set, 1, texture);

  if (got_new_size)
    {
      G3kPlaneClass *klass = G3K_PLANE_GET_CLASS (self);
      klass->update_texture_size (self);
    }
}

GulkanTexture *
g3k_plane_get_texture (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  return priv->texture;
}

gboolean
g3k_plane_has_texture (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  return priv->texture != NULL;
}

void
g3k_plane_update_mesh (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  G3kPlaneClass   *klass = G3K_PLANE_GET_CLASS (self);
  g_clear_object (&priv->vertex_buffer);
  priv->vertex_buffer = klass->init_mesh (self);
}

void
g3k_plane_store_texture (G3kPlane *self, GulkanTexture *texture)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  priv->texture = texture;
}

GulkanVertexBuffer *
g3k_plane_get_vertex_buffer (G3kPlane *self)
{
  G3kPlanePrivate *priv = g3k_plane_get_instance_private (self);
  return priv->vertex_buffer;
}

static void
_get_normal (G3kPlane *self, graphene_vec3_t *normal)
{
  graphene_vec3_init (normal, 0, 0, 1);

  graphene_matrix_t model_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &model_matrix);

  graphene_matrix_t rotation_matrix;

  graphene_point3d_t unused_scale;
  graphene_ext_matrix_get_rotation_matrix (&model_matrix, &unused_scale,
                                           &rotation_matrix);

  graphene_matrix_transform_vec3 (&rotation_matrix, normal, normal);
}

static void
_get_plane (G3kPlane *self, graphene_plane_t *res)
{
  graphene_vec3_t normal;
  _get_normal (self, &normal);

  graphene_matrix_t model_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &model_matrix);

  graphene_point3d_t position;
  graphene_ext_matrix_get_translation_point3d (&model_matrix, &position);

  graphene_plane_init_from_point (res, &normal, &position);
}

static gboolean
_get_intersection (G3kObject          *object,
                   graphene_ray_t     *ray,
                   float              *distance,
                   graphene_point3d_t *res)
{
  G3kPlane *self = G3K_PLANE (object);

  graphene_plane_t plane;
  _get_plane (self, &plane);

  graphene_matrix_t window_transform;
  g3k_object_get_matrix (G3K_OBJECT (self), &window_transform);

  *distance = graphene_ray_get_distance_to_plane (ray, &plane);
  if (*distance == INFINITY)
    return FALSE;

  graphene_vec3_t res_vec;

  graphene_ray_get_direction (ray, &res_vec);
  graphene_vec3_scale (&res_vec, *distance, &res_vec);

  graphene_vec3_t origin;
  graphene_ext_ray_get_origin_vec3 (ray, &origin);
  graphene_vec3_add (&origin, &res_vec, &res_vec);

  graphene_matrix_t inverse_plane_transform;
  graphene_matrix_inverse (&window_transform, &inverse_plane_transform);

  graphene_vec4_t intersection_vec4;
  graphene_vec4_init_from_vec3 (&intersection_vec4, &res_vec, 1.0f);

  // cancels g3k object scale which is implicit in window transform
  graphene_vec4_t intersection_plane_space;
  graphene_matrix_transform_vec4 (&inverse_plane_transform, &intersection_vec4,
                                  &intersection_plane_space);

  graphene_point3d_init_from_vec3 (res, &res_vec);

  float x = graphene_vec4_get_x (&intersection_plane_space);
  float y = graphene_vec4_get_y (&intersection_plane_space);

  graphene_size_t size_meters = g3k_plane_get_mesh_size (G3K_PLANE (self));
  if (x >= -size_meters.width / 2.f && x <= size_meters.width / 2.f
      && y >= -size_meters.height / 2.f && y <= size_meters.height / 2.f)
    {
      return TRUE;
    }

  return FALSE;
}

float
g3k_plane_get_global_ppm (G3kPlane *self)
{
  graphene_size_t global_size = g3k_plane_get_global_size_meters (self);
  VkExtent2D      extent = g3k_plane_get_content_extent (self);
  return g3k_to_ppm ((int32_t) extent.width, global_size.width);
}

/**
 * g3k_plane_get_global_size_meters:
 * @self: The #G3kPlane
 *
 * Returns: The current world space width of the window in meters.
 */
graphene_size_t
g3k_plane_get_global_size_meters (G3kPlane *self)
{
  graphene_point3d_t global_scale;
  g3k_object_get_scale (G3K_OBJECT (self), &global_scale);

  graphene_size_t mesh_size = g3k_plane_get_mesh_size (self);

  graphene_size_t global_size = {global_scale.x * mesh_size.width,
                                 global_scale.y * mesh_size.height};

  return global_size;
}

VkExtent2D
g3k_plane_get_content_extent (G3kPlane *self)
{
  G3kPlaneClass *klass = G3K_PLANE_GET_CLASS (self);
  return klass->get_content_extent (self);
}
