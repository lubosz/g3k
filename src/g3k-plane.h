/*
 * g3k
 * Copyright 2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_PLANE_H_
#define G3K_PLANE_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <stdalign.h>

#include "g3k-object.h"

G_BEGIN_DECLS

#define G3K_TYPE_PLANE g3k_plane_get_type ()
G_DECLARE_DERIVABLE_TYPE (G3kPlane, g3k_plane, G3K, PLANE, G3kObject)

/**
 * G3kPlaneClass:
 * @parent: The object class structure needs to be the first
 *   element in the widget class structure in order for the class mechanism
 *   to work correctly. This allows a G3kPlaneClass pointer to be cast to
 *   a G3kObjectClass pointer.
 */
struct _G3kPlaneClass
{
  G3kObjectClass parent;

  GulkanVertexBuffer *(*init_mesh) (G3kPlane *self);

  void (*update_texture_size) (G3kPlane *self);

  VkExtent2D (*get_content_extent) (G3kPlane *self);
};

typedef struct
{
  alignas (32) float mvp[2][16];
} G3kPlaneUniformBuffer;

G3kPlane *
g3k_plane_new (void);

void
g3k_plane_set_mesh_size (G3kPlane *self, graphene_size_t *size_meters);

graphene_size_t
g3k_plane_get_mesh_size (G3kPlane *self);

void
g3k_plane_set_texture (G3kPlane *self, GulkanTexture *texture);

GulkanTexture *
g3k_plane_get_texture (G3kPlane *self);

gboolean
g3k_plane_has_texture (G3kPlane *self);

float
g3k_plane_get_global_ppm (G3kPlane *self);

graphene_size_t
g3k_plane_get_global_size_meters (G3kPlane *self);

VkExtent2D
g3k_plane_get_content_extent (G3kPlane *self);

GulkanVertexBuffer *
g3k_plane_get_vertex_buffer (G3kPlane *self);

G_END_DECLS

#endif /* G3K_PLANE_H_ */
