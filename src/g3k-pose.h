/*
 * xrdesktop
 * Copyright 2022 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_POSE_H_
#define G3K_POSE_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib.h>
#include <graphene.h>

typedef struct
{
  graphene_point3d_t    position;
  graphene_quaternion_t rotation;
} G3kPose;

void
g3k_pose_init_from_pose (G3kPose *pose, G3kPose *other);

G3kPose
g3k_pose_new (graphene_point3d_t *position, graphene_quaternion_t *rotation);

G3kPose
g3k_pose_new_from_matrix (graphene_matrix_t *m);

void
g3k_pose_to_matrix (G3kPose *pose, graphene_matrix_t *out_transform);

gboolean
g3k_pose_validate (G3kPose *pose);

#endif
