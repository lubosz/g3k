/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-ray.h"

#include <stdalign.h>

#include "g3k-renderer.h"
#include "g3k-settings.h"

typedef struct
{
  alignas (32) float mvp[2][16];
} G3kRayUniformBuffer;

struct _G3kRay
{
  G3kObject           parent;
  GulkanVertexBuffer *vertex_buffer;

  float    start_offset;
  float    length;
  float    default_length;
  gboolean render_ray;
};

G_DEFINE_TYPE (G3kRay, g3k_ray, G3K_TYPE_OBJECT)

static void
g3k_ray_finalize (GObject *gobject);

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer);

static void
g3k_ray_class_init (G3kRayClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = g3k_ray_finalize;

  G3kObjectClass *g3k_object_class = G3K_OBJECT_CLASS (klass);
  g3k_object_class->draw = _draw;
}

static void
g3k_ray_init (G3kRay *self)
{
  self->start_offset = -0.02f;
  self->default_length = 5.0f;
  self->length = self->default_length;
}

static void
_update_render_ray (GSettings *settings, gchar *key, gpointer _data)
{
  G3kRay *self = G3K_RAY (_data);
  self->render_ray = g_settings_get_boolean (settings, key);

  if (!g3k_object_is_visible (G3K_OBJECT (self)) && self->render_ray)
    g3k_ray_show (self);
  else if (g3k_object_is_visible (G3K_OBJECT (self)) && !self->render_ray)
    g3k_object_set_visibility (G3K_OBJECT (self), FALSE);
}

static gboolean
_initialize (G3kRay *self, G3kContext *g3k)
{
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);
  self->vertex_buffer
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
  gulkan_vertex_buffer_reset (self->vertex_buffer);

  graphene_vec4_t start;
  graphene_vec4_init (&start, 0, 0, self->start_offset, 1);

  graphene_matrix_t identity;
  graphene_matrix_init_identity (&identity);

  gulkan_geometry_append_ray (self->vertex_buffer, &start, self->length,
                              &identity);

  if (!gulkan_vertex_buffer_alloc_empty (self->vertex_buffer,
                                         GXR_DEVICE_INDEX_MAX))
    return FALSE;

  gulkan_vertex_buffer_map_array (self->vertex_buffer);

  G3kObject *obj = G3K_OBJECT (self);

  VkDeviceSize ubo_size = sizeof (G3kRayUniformBuffer);

  G3kRenderer *renderer = g3k_context_get_renderer (g3k);
  G3kPipeline *pipeline = g3k_renderer_get_pipeline (renderer, "pointer");

  if (!g3k_object_initialize (obj, g3k, pipeline, ubo_size))
    return FALSE;

  g3k_settings_connect_and_apply (G_CALLBACK (_update_render_ray), "org.g3k",
                                  "pointer-ray-enabled", self);

  return TRUE;
}

G3kRay *
g3k_ray_new (G3kContext *g3k)
{
  G3kRay *self = (G3kRay *) g_object_new (G3K_TYPE_RAY, 0);
  _initialize (self, g3k);
  return self;
}

static void
g3k_ray_finalize (GObject *gobject)
{
  G3kRay *self = G3K_RAY (gobject);
  g_clear_object (&self->vertex_buffer);
  G_OBJECT_CLASS (g3k_ray_parent_class)->finalize (gobject);
}

static void
_update_ubo (G3kRay *self)
{
  G3kRayUniformBuffer ub = {0};

  graphene_matrix_t m_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &m_matrix);

  G3kContext        *context = g3k_object_get_context (G3K_OBJECT (self));
  graphene_matrix_t *vp = g3k_context_get_vps (context);

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_matrix_t mvp_matrix;
      graphene_matrix_multiply (&m_matrix, &vp[eye], &mvp_matrix);
      graphene_matrix_to_float (&mvp_matrix, ub.mvp[eye]);
    }

  g3k_object_update_transformation_ubo (G3K_OBJECT (self), &ub);
}

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer)
{
  G3kRay *self = G3K_RAY (obj);

  if (!gulkan_vertex_buffer_is_initialized (self->vertex_buffer))
    return;

  _update_ubo (self);

  g3k_object_bind (obj, cmd_buffer);
  gulkan_vertex_buffer_draw (self->vertex_buffer, cmd_buffer);
}

void
g3k_ray_set_length (G3kRay *self, float length)
{
  if (length == self->length)
    return;

  self->length = length;

  gulkan_vertex_buffer_reset (self->vertex_buffer);

  graphene_matrix_t identity;
  graphene_matrix_init_identity (&identity);

  graphene_vec4_t start;
  graphene_vec4_init (&start, 0, 0, self->start_offset, 1);

  gulkan_geometry_append_ray (self->vertex_buffer, &start, length, &identity);
  gulkan_vertex_buffer_map_array (self->vertex_buffer);
}

float
g3k_ray_get_default_length (G3kRay *self)
{
  return self->default_length;
}

void
g3k_ray_reset_length (G3kRay *self)
{
  g3k_ray_set_length (self, self->default_length);
}

gboolean
g3k_ray_get_intersection (G3kRay             *self,
                          G3kObject          *object,
                          float              *distance,
                          graphene_point3d_t *res)
{
  graphene_matrix_t mat;
  g3k_object_get_matrix (G3K_OBJECT (self), &mat);

  graphene_vec4_t start;
  graphene_vec4_init (&start, 0, 0, self->start_offset, 1);
  graphene_matrix_transform_vec4 (&mat, &start, &start);

  graphene_vec4_t end;
  graphene_vec4_init (&end, 0, 0, -self->length, 1);
  graphene_matrix_transform_vec4 (&mat, &end, &end);

  graphene_vec4_t direction_vec4;
  graphene_vec4_subtract (&end, &start, &direction_vec4);

  graphene_point3d_t origin;
  graphene_vec3_t    direction;

  graphene_vec3_t vec3_start;
  graphene_vec4_get_xyz (&start, &vec3_start);
  graphene_point3d_init_from_vec3 (&origin, &vec3_start);

  graphene_vec4_get_xyz (&direction_vec4, &direction);

  graphene_ray_t ray;
  graphene_ray_init (&ray, &origin, &direction);

  return g3k_object_get_intersection (object, &ray, distance, res);
}

void
g3k_ray_show (G3kRay *self)
{
  if (self->render_ray)
    g3k_object_set_visibility (G3K_OBJECT (self), TRUE);
}
