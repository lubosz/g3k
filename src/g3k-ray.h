/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_RAY_H_
#define G3K_RAY_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-object.h"

G_BEGIN_DECLS

#define G3K_TYPE_RAY g3k_ray_get_type ()
G_DECLARE_FINAL_TYPE (G3kRay, g3k_ray, G3K, RAY, G3kObject)

G3kRay *
g3k_ray_new (G3kContext *g3k);

void
g3k_ray_set_length (G3kRay *self, float length);

float
g3k_ray_get_default_length (G3kRay *self);

void
g3k_ray_reset_length (G3kRay *self);

gboolean
g3k_ray_get_intersection (G3kRay             *self,
                          G3kObject          *object,
                          float              *distance,
                          graphene_point3d_t *res);

void
g3k_ray_show (G3kRay *self);

G_END_DECLS

#endif /* G3K_RAY_H_ */
