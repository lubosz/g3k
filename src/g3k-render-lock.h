/*
 * xrdesktop
 * Copyright 2020 Collabora Ltd.
 * Author: Chrtstoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_RENDER_LOCK_H_
#define G3K_RENDER_LOCK_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

void
g3k_render_lock_init (void);

void
g3k_render_lock_destroy (void);

void
g3k_render_lock (void);

void
g3k_render_unlock (void);

#endif // G3K_RENDER_LOCK_H_
