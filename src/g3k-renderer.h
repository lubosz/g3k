/*
 * xrddesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_RENDERER_H_
#define G3K_RENDERER_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-pipeline.h"

G_BEGIN_DECLS

#define G3K_TYPE_RENDERER g3k_renderer_get_type ()
G_DECLARE_FINAL_TYPE (G3kRenderer, g3k_renderer, G3K, RENDERER, GulkanRenderer)

G3kRenderer *
g3k_renderer_new (G3kContext *context);

GulkanRenderPass *
g3k_renderer_get_render_pass (G3kRenderer *self);

gboolean
g3k_renderer_draw (G3kRenderer *self);

GulkanUniformBuffer *
g3k_renderer_get_lights_buffer (G3kRenderer *self);

G3kPipeline *
g3k_renderer_get_pipeline (G3kRenderer *self, const gchar *name);

VkSampleCountFlagBits
g3k_renderer_get_sample_count (G3kRenderer *self);

G_END_DECLS

#endif /* G3K_RENDERER_H_ */
