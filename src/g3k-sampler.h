/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_SAMPLER_H_
#define G3K_SAMPLER_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>
#include <gulkan.h>

G_BEGIN_DECLS

struct _G3kSampler
{
  GObject parent;

  VkFilter             mag_filter;
  VkFilter             min_filter;
  VkSamplerAddressMode wrap_s;
  VkSamplerAddressMode wrap_t;
  VkSamplerMipmapMode  mipmap_mode;
};

#define G3K_TYPE_SAMPLER g3k_sampler_get_type ()
G_DECLARE_FINAL_TYPE (G3kSampler, g3k_sampler, G3K, SAMPLER, GObject)

G3kSampler *
g3k_sampler_new (void);

G_END_DECLS

#endif /* G3K_SAMPLER_H_ */
