/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_SELECTION_H_
#define G3K_SELECTION_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-object.h"

G_BEGIN_DECLS

#define G3K_TYPE_SELECTION g3k_selection_get_type ()
G_DECLARE_FINAL_TYPE (G3kSelection, g3k_selection, G3K, SELECTION, G3kObject)

G3kSelection *
g3k_selection_new (G3kContext *g3k);

void
g3k_selection_set_quad (G3kSelection *self, float width, float height);

G_END_DECLS

#endif /* G3K_SELECTION_H_ */
