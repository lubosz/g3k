/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-tip.h"

#include <gdk/gdk.h>
#include <graphene-ext.h>

#include "g3k-math.h"
#include "g3k-renderer.h"
#include "g3k-settings.h"

/*
 * Since the pulse animation surrounds the tip and would
 * exceed the canvas size, we need to scale it to fit the pulse.
 */
#define GXR_TIP_VIEWPORT_SCALE 3

#define TIP_Z_OFFSET 0.001f

/*
 * The distance in meters for which apparent size and regular size are equal.
 */
#define GXR_TIP_APPARENT_SIZE_DISTANCE 3.0f

typedef struct
{
  G3kTip *tip;
  float   progress;
  guint   callback_id;
} G3kTipAnimation;

typedef struct
{
  gboolean keep_apparent_size;
  float    width_meters;

  graphene_point3d_t active_color;
  graphene_point3d_t passive_color;

  double pulse_alpha;

  int texture_width;
  int texture_height;
} G3kTipSettings;

struct _G3kTip
{
  G3kPlane parent;

  gboolean active;

  G3kTipSettings settings;

  /* Pointer to the data of the currently running animation.
   * Must be freed when an animation callback is cancelled. */
  G3kTipAnimation *animation;
};

G_DEFINE_TYPE (G3kTip, g3k_tip, G3K_TYPE_PLANE)

static GdkPixbuf *
_render_texture (G3kTip *self, float progress);

static GulkanTexture *
_create_texture (G3kTip *self);

static void
_update_texture (G3kTip *self);

static gboolean
_cancel_animation (G3kTip *self);

static void
_update_texture_res (GSettings *settings, gchar *key, gpointer _data)
{
  G3kTip         *self = G3K_TIP (_data);
  G3kTipSettings *s = &self->settings;

  GVariant *texture_res = g_settings_get_value (settings, key);
  g_variant_get (texture_res, "(ii)", &s->texture_width, &s->texture_height);
  g_variant_unref (texture_res);

  GulkanTexture *texture = _create_texture (self);
  g3k_plane_set_texture (G3K_PLANE (self), texture);
}

static void
_update_active_color (GSettings *settings, gchar *key, gpointer _data)
{
  G3kTip         *self = G3K_TIP (_data);
  G3kTipSettings *s = &self->settings;

  GVariant *var = g_settings_get_value (settings, key);
  double    r, g, b;
  g_variant_get (var, "(ddd)", &r, &g, &b);
  graphene_point3d_init (&s->active_color, (float) r, (float) g, (float) b);
  g_variant_unref (var);

  if (self->active)
    {
      _cancel_animation (self);
      _update_texture (self);
    }
}

static void
_update_passive_color (GSettings *settings, gchar *key, gpointer _data)
{
  G3kTip         *self = G3K_TIP (_data);
  G3kTipSettings *s = &self->settings;

  GVariant *var = g_settings_get_value (settings, key);
  double    r, g, b;
  g_variant_get (var, "(ddd)", &r, &g, &b);
  graphene_point3d_init (&s->passive_color, (float) r, (float) g, (float) b);
  g_variant_unref (var);

  if (!self->active)
    {
      _cancel_animation (self);
      _update_texture (self);
    }
}

static void
_update_pulse_alpha (GSettings *settings, gchar *key, gpointer _data)
{
  G3kTip *self = G3K_TIP (_data);
  self->settings.pulse_alpha = g_settings_get_double (settings, key);
}

static void
_update_keep_apparent_size (GSettings *settings, gchar *key, gpointer _data)
{
  G3kTip *self = G3K_TIP (_data);
  self->settings.keep_apparent_size = g_settings_get_boolean (settings, key);
}

static void
_update_width_meters (GSettings *settings, gchar *key, gpointer _data)
{
  G3kTip *self = G3K_TIP (_data);
  float   width_meters = (float) g_settings_get_double (settings, key);
  self->settings.width_meters = width_meters * GXR_TIP_VIEWPORT_SCALE;
  graphene_size_t size = {
    .width = self->settings.width_meters,
    .height = self->settings.width_meters,
  };
  g3k_plane_set_mesh_size (G3K_PLANE (self), &size);
}

static void
g3k_tip_init (G3kTip *self)
{
  self->active = FALSE;
  self->animation = NULL;

  G3kTipSettings *s = &self->settings;
  s->keep_apparent_size = TRUE;
  s->width_meters = 0.05f * GXR_TIP_VIEWPORT_SCALE;
  graphene_point3d_init (&s->active_color, 0.078f, 0.471f, 0.675f);
  graphene_point3d_init (&s->passive_color, 1.0f, 1.0f, 1.0f);
  s->texture_width = 64;
  s->texture_height = 64;
  s->pulse_alpha = 0.25;
}

static gboolean
_initialize (G3kTip *self, G3kContext *g3k)
{
  VkDeviceSize ubo_size = sizeof (G3kPlaneUniformBuffer);

  G3kRenderer *renderer = g3k_context_get_renderer (g3k);
  G3kPipeline *pipeline = g3k_renderer_get_pipeline (renderer, "tip");

  if (!g3k_object_initialize (G3K_OBJECT (self), g3k, pipeline, ubo_size))
    return FALSE;

  /* tip resolution config has to happen after initialize */
  g3k_settings_connect_and_apply (G_CALLBACK (_update_texture_res), "org.g3k",
                                  "pointer-tip-resolution", self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_passive_color), "org.g3k",
                                  "pointer-tip-passive-color", self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_active_color), "org.g3k",
                                  "pointer-tip-active-color", self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_pulse_alpha), "org.g3k",
                                  "pointer-tip-pulse-alpha", self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_keep_apparent_size),
                                  "org.g3k", "pointer-tip-keep-apparent-size",
                                  self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_width_meters), "org.g3k",
                                  "pointer-tip-width-meters", self);

  return TRUE;
}

G3kTip *
g3k_tip_new (G3kContext *g3k)
{
  G3kTip *self = (G3kTip *) g_object_new (G3K_TYPE_TIP, 0);
  if (!_initialize (self, g3k))
    {
      g_printerr ("Could not initialize G3kTip.\n");
      g_object_unref (self);
      return NULL;
    }
  return self;
}

static void
_finalize (GObject *gobject)
{
  G3kTip *self = G3K_TIP (gobject);

  /* cancels potentially running animation */
  g3k_tip_set_active (G3K_TIP (self), FALSE);

  G_OBJECT_CLASS (g3k_tip_parent_class)->finalize (gobject);
}

/**
 * g3k_tip_update_apparent_size:
 * @self: a #G3kTip
 * @context: a #GxrContext
 *
 * Note: Move pointer tip to the desired location before calling.
 */
void
g3k_tip_update_apparent_size (G3kTip *self)
{
  if (!self->settings.keep_apparent_size)
    return;

  graphene_point3d_t position;
  graphene_matrix_t  g;
  g3k_object_get_matrix (G3K_OBJECT (self), &g);
  graphene_ext_matrix_get_translation_point3d (&g, &position);

  graphene_matrix_t hmd_pose;

  G3kContext *g3k = g3k_object_get_context (G3K_OBJECT (self));
  GxrContext *gxr = g3k_context_get_gxr (g3k);

  gboolean has_pose = gxr_context_get_head_pose (gxr, &hmd_pose);
  if (!has_pose)
    {
      g3k_object_reset_local_scale (G3K_OBJECT (self));
      return;
    }

  graphene_point3d_t hmd_point;
  graphene_ext_matrix_get_translation_point3d (&hmd_pose, &hmd_point);

  float distance = graphene_point3d_distance (&position, &hmd_point, NULL);

  /* divide distance by 3 so the width and the apparent width are the same at
   * a distance of 3 meters. This makes e.g. self->width = 0.3 look decent in
   * both cases at typical usage distances. */
  float w = distance / GXR_TIP_APPARENT_SIZE_DISTANCE;

  graphene_point3d_t apparent_scale = {.x = w, .y = w, .z = 1.0f};
  g3k_object_set_local_scale (G3K_OBJECT (self), &apparent_scale);
}

static gboolean
_cancel_animation (G3kTip *self)
{
  if (self->animation != NULL)
    {
      g_source_remove (self->animation->callback_id);
      g_free (self->animation);
      self->animation = NULL;
      return TRUE;
    }
  else
    return FALSE;
}

static GulkanTexture *
_create_texture (G3kTip *self)
{
  G3kContext    *g3k = g3k_object_get_context (G3K_OBJECT (self));
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);

  GdkPixbuf *pixbuf = _render_texture (self, 1.0f);

  GulkanTexture *texture
    = gulkan_texture_new_from_pixbuf (gulkan, pixbuf, VK_FORMAT_R8G8B8A8_SRGB,
                                      g3k_context_get_upload_layout (g3k),
                                      false);
  g_object_unref (pixbuf);

  return texture;
}

/* draws a circle in the center of a cairo surface of dimensions WIDTHxHEIGHT.
 * scale affects the radius of the circle and should be in [0,2].
 * a_in is the alpha value at the center, a_out at the outer border. */
static void
_draw_gradient_circle (cairo_t            *cr,
                       int                 w,
                       int                 h,
                       double              radius,
                       graphene_point3d_t *color,
                       double              a_in,
                       double              a_out)
{
  double center_x = w / 2;
  double center_y = h / 2;

  cairo_pattern_t *pat = cairo_pattern_create_radial (center_x, center_y,
                                                      0.75 * radius, center_x,
                                                      center_y, radius);
  cairo_pattern_add_color_stop_rgba (pat, 0, (double) color->x,
                                     (double) color->y, (double) color->z,
                                     a_in);

  cairo_pattern_add_color_stop_rgba (pat, 1, (double) color->x,
                                     (double) color->y, (double) color->z,
                                     a_out);
  cairo_set_source (cr, pat);
  cairo_arc (cr, center_x, center_y, radius, 0, (double) (2.0f * M_PI));
  cairo_fill (cr);
  cairo_pattern_destroy (pat);
}

static GdkPixbuf *
_render_cairo (int                 w,
               int                 h,
               double              radius,
               graphene_point3d_t *color,
               double              pulse_alpha,
               float               progress)
{
  cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, w,
                                                         h);

  cairo_t *cr = cairo_create (surface);
  cairo_set_source_rgba (cr, 0, 0, 0, 0);
  cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
  cairo_paint (cr);

  /* Draw pulse */
  if (progress != 1.0f)
    {
      float pulse_scale = GXR_TIP_VIEWPORT_SCALE * (1.0f - progress);
      graphene_point3d_t white = {1.0f, 1.0f, 1.0f};
      _draw_gradient_circle (cr, w, h, radius * (double) pulse_scale, &white,
                             pulse_alpha, 0.0);
    }

  cairo_set_operator (cr, CAIRO_OPERATOR_MULTIPLY);

  /* Draw tip */
  _draw_gradient_circle (cr, w, h, radius, color, 1.0, 0.0);

  cairo_destroy (cr);

  /* Since we cannot set a different format for raw upload,
   * we need to use GdkPixbuf to suit OpenVRs needs */
  GdkPixbuf *pixbuf = gdk_pixbuf_get_from_surface (surface, 0, 0, w, h);

  cairo_surface_destroy (surface);

  return pixbuf;
}

/** _render:
 * Renders the pointer tip with the desired colors.
 * If background scale is > 1, a transparent white background circle is rendered
 * behind the pointer tip. */
static GdkPixbuf *
_render_texture (G3kTip *self, float progress)
{
  int w = self->settings.texture_width * GXR_TIP_VIEWPORT_SCALE;
  int h = self->settings.texture_height * GXR_TIP_VIEWPORT_SCALE;

  g_assert (w != 0 && h != 0);

  graphene_point3d_t *color = self->active ? &self->settings.active_color
                                           : &self->settings.passive_color;

  double radius = self->settings.texture_width / 2.0;

  GdkPixbuf *pixbuf = _render_cairo (w, h, radius, color,
                                     self->settings.pulse_alpha, progress);

  return pixbuf;
}

static gboolean
_animate_cb (gpointer _animation)
{
  G3kTipAnimation *animation = (G3kTipAnimation *) _animation;
  G3kTip          *tip = animation->tip;

  GulkanTexture *texture = g3k_plane_get_texture (G3K_PLANE (tip));

  GdkPixbuf *pixbuf = _render_texture (tip, animation->progress);

  G3kContext *g3k = g3k_object_get_context (G3K_OBJECT (tip));
  gulkan_texture_upload_pixbuf (texture, pixbuf,
                                g3k_context_get_upload_layout (g3k));

  g_object_unref (pixbuf);

  animation->progress += 0.05f;

  if (animation->progress > 1)
    {
      tip->animation = NULL;
      g_free (animation);
      return FALSE;
    }

  return TRUE;
}

void
g3k_tip_animate_pulse (G3kTip *self)
{
  if (self->animation != NULL)
    g3k_tip_set_active (self, self->active);

  self->animation = g_malloc (sizeof (G3kTipAnimation));
  self->animation->progress = 0;
  self->animation->tip = self;
  self->animation->callback_id = g_timeout_add (20, _animate_cb,
                                                self->animation);
}

static void
_update_texture (G3kTip *self)
{
  GdkPixbuf     *pixbuf = _render_texture (self, 1.0f);
  GulkanTexture *texture = g3k_plane_get_texture (G3K_PLANE (self));

  G3kContext *g3k = g3k_object_get_context (G3K_OBJECT (self));
  gulkan_texture_upload_pixbuf (texture, pixbuf,
                                g3k_context_get_upload_layout (g3k));

  g_object_unref (pixbuf);
}

/**
 * g3k_tip_set_active:
 * @self: a #G3kTip
 * @active: whether to use the active or inactive style
 *
 * Changes whether the active or inactive style is rendered.
 * Also cancels animations.
 */
void
g3k_tip_set_active (G3kTip *self, gboolean active)
{
  if (!g3k_plane_has_texture (G3K_PLANE (self)))
    return;

  /* New texture needs to be rendered when
   *  - animation is being cancelled
   *  - active status changes
   * Otherwise the texture should already show the current active status. */

  gboolean animation_cancelled = _cancel_animation (self);
  if (!animation_cancelled && self->active == active)
    return;

  self->active = active;

  _update_texture (self);
}

static void
g3k_tip_class_init (G3kTipClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}
