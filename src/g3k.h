/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_H
#define G3K_H

#define G3K_INSIDE

#include "g3k-background.h"
#include "g3k-button.h"
#include "g3k-container.h"
#include "g3k-cursor.h"
#include "g3k-keyboard-button.h"
#include "g3k-keyboard.h"
#include "g3k-loader.h"
#include "g3k-math.h"
#include "g3k-object-manager.h"
#include "g3k-object.h"
#include "g3k-pipeline.h"
#include "g3k-plane.h"
#include "g3k-ppm.h"
#include "g3k-ray.h"
#include "g3k-render-lock.h"
#include "g3k-renderer.h"
#include "g3k-selection.h"
#include "g3k-settings.h"
#include "g3k-tip.h"
#include "g3k-types.h"
#include "g3k-version.h"

#undef G3K_INSIDE

#endif /* G3K_H */
