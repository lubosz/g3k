/*
 * xrdesktop
 * Copyright 2021 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <g3k.h>
#include <glib.h>
#include <graphene-ext.h>

#define EPSILON 0.0001f

static gboolean
_check_point3d_t (graphene_point3d_t *pt, float x, float y, float z)
{
  g_assert_cmpfloat_with_epsilon (pt->x, x, EPSILON);
  g_assert_cmpfloat_with_epsilon (pt->y, y, EPSILON);
  g_assert_cmpfloat_with_epsilon (pt->z, z, EPSILON);
  return TRUE;
}

static gboolean
_check_global_position (G3kObject *object, float x, float y, float z)
{
  graphene_point3d_t global_position;
  g3k_object_get_position (object, &global_position);
  return _check_point3d_t (&global_position, x, y, z);
}

static gboolean
_check_global_scale (G3kObject *object, float x, float y, float z)
{
  graphene_point3d_t scale;
  g3k_object_get_scale (object, &scale);
  return _check_point3d_t (&scale, x, y, z);
}

static gboolean
_check_relative_scale (G3kObject *object, float x, float y, float z)
{
  graphene_point3d_t scale;
  g3k_object_get_local_scale (object, &scale);

  return _check_point3d_t (&scale, x, y, z);
}

static gboolean
_check_quat (graphene_quaternion_t *q, float x, float y, float z)
{
  graphene_quaternion_t in_q;
  graphene_quaternion_init_from_angles (&in_q, x, y, z);

  if (!graphene_quaternion_equal (&in_q, q))
    {
      float rot_x;
      float rot_y;
      float rot_z;
      graphene_quaternion_to_angles (q, &rot_x, &rot_y, &rot_z);
      g_assert_cmpfloat_with_epsilon (rot_x, x, EPSILON);
      g_assert_cmpfloat_with_epsilon (rot_y, y, EPSILON);
      g_assert_cmpfloat_with_epsilon (rot_z, z, EPSILON);
    }
  return TRUE;
}

static gboolean
_check_global_rotation (G3kObject *object, float x, float y, float z)
{
  G3kPose pose = g3k_object_get_pose (object);
  _check_quat (&pose.rotation, x, y, z);
  return TRUE;
}

static graphene_matrix_t
_make_translation_matrix (float x, float y, float z)
{
  graphene_point3d_t position = {x, y, z};
  graphene_matrix_t  transform;
  graphene_matrix_init_translate (&transform, &position);
  return transform;
}

static graphene_matrix_t
_make_rotation_matrix (float x, float y, float z)
{
  graphene_quaternion_t q;
  graphene_quaternion_init_from_angles (&q, x, y, z);

  graphene_matrix_t transform;
  graphene_matrix_init_identity (&transform);
  graphene_matrix_rotate_quaternion (&transform, &q);

  return transform;
}

static void
_reset_object (G3kObject *object)
{
  graphene_matrix_t identity;
  graphene_matrix_init_identity (&identity);
  g3k_object_set_local_matrix (object, &identity);
}

static void
_reset_all (G3kObject *root)
{
  GQueue *next = g_queue_new ();
  g_queue_push_head (next, root);

  while (!g_queue_is_empty (next))
    {
      G3kObject *object = G3K_OBJECT (g_queue_pop_head (next));
      _reset_object (object);

      GSList *children = g3k_object_get_children (object);
      for (GSList *l = children; l; l = l->next)
        {
          G3kObject *child = G3K_OBJECT (l->data);
          g_queue_push_head (next, child);
        }
    }

  g_queue_free (next);
}

static gboolean
_test_child_offset (G3kObject *root, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t child_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_local_matrix (child, &child_transform);

  _check_global_position (child, 1, 2, 3);
  _check_global_rotation (child, 0, 0, 0);

  return TRUE;
}

static gboolean
_test_parent_offset (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_local_matrix (parent, &local_transform);

  _check_global_position (child, 1, 2, 3);
  _check_global_rotation (child, 0, 0, 0);

  return TRUE;
}

static gboolean
_test_double_offset (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_local_matrix (parent, &local_transform);

  graphene_matrix_t child_transform = _make_translation_matrix (4, 5, 6);
  g3k_object_set_local_matrix (child, &child_transform);

  _check_global_position (child, 5, 7, 9);
  _check_global_rotation (child, 0, 0, 0);

  return TRUE;
}

static gboolean
_test_child_rotation (G3kObject *root, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t child_transform = _make_rotation_matrix (0, 90, 45);

  g3k_object_set_local_matrix (child, &child_transform);

  _check_global_position (child, 0, 0, 0);
  _check_global_rotation (child, 0, 90, 45);

  return TRUE;
}

static gboolean
_test_parent_rotation (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_rotation_matrix (0, 90, 0);

  g3k_object_set_local_matrix (parent, &local_transform);

  _check_global_position (child, 0, 0, 0);
  _check_global_rotation (child, 0, 90, 0);

  return TRUE;
}

static gboolean
_test_parent_rotation_parent_offset (G3kObject *root,
                                     G3kObject *parent,
                                     G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t  local_transform = _make_rotation_matrix (0, 90, 0);
  graphene_point3d_t parent_offset = {1, 2, 3};
  graphene_ext_matrix_set_translation_point3d (&local_transform,
                                               &parent_offset);

  g3k_object_set_local_matrix (parent, &local_transform);

  _check_global_position (child, 1, 2, 3);
  _check_global_rotation (child, 0, 90, 0);

  return TRUE;
}

static gboolean
_test_parent_rotation_child_offset (G3kObject *root,
                                    G3kObject *parent,
                                    G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_rotation_matrix (0, 90, 0);
  g3k_object_set_local_matrix (parent, &local_transform);

  graphene_matrix_t child_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_local_matrix (child, &child_transform);

  _check_global_position (child, 3, 2, -1);
  _check_global_rotation (child, 0, 90, 0);

  return TRUE;
}

static gboolean
_test_parent_neg_rotation_child_offset (G3kObject *root,
                                        G3kObject *parent,
                                        G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_rotation_matrix (0, -90, 0);
  g3k_object_set_local_matrix (parent, &local_transform);

  graphene_matrix_t child_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_local_matrix (child, &child_transform);

  _check_global_position (child, -3, 2, 1);
  _check_global_rotation (child, 0, 90, 0);

  return TRUE;
}

static gboolean
_test_global_scale (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_point3d_t global_parent_scale;
  g3k_object_get_scale (parent, &global_parent_scale);

  // all scales should default to 1.0
  _check_global_scale (parent, 1.0f, 1.0f, 1.0f);

  graphene_point3d_t local_scale = {0.5f, 0.1f, 0.5f};
  g3k_object_set_local_scale (parent, &local_scale);

  g3k_object_get_scale (parent, &global_parent_scale);
  _check_global_scale (parent, 0.5f, 0.1f, 0.5f);

  graphene_point3d_t global_child_scale = {1, 2, 3};
  g3k_object_set_scale (child, &global_child_scale);

  g3k_object_get_scale (child, &global_child_scale);
  _check_global_scale (child, 1, 2, 3);

  graphene_point3d_t relative_child_scale;
  g3k_object_get_scale (child, &relative_child_scale);

  // parent scaled 0.5, 0.1, 0.5.
  // Child should be globally scaled to 1, 2, 3.
  _check_relative_scale (child, 2, 20, 6);

  // scaling shouldn't change the poses when everything is at 0,0
  _check_global_position (child, 0, 0, 0);
  _check_global_rotation (child, 0, 0, 0);

  return TRUE;
}

static gboolean
_test_parent_scale (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_point3d_t global_parent_scale;
  g3k_object_get_scale (parent, &global_parent_scale);

  // all scales should default to 1.0
  _check_global_scale (parent, 1.0f, 1.0f, 1.0f);

  graphene_point3d_t local_scale = {0.5f, 0.4f, 0.3f};
  g3k_object_set_local_scale (parent, &local_scale);

  g3k_object_get_scale (parent, &global_parent_scale);
  _check_global_scale (parent, 0.5f, 0.4f, 0.3f);

  // Check that the scale propagated to the child
  graphene_point3d_t global_child_scale;
  g3k_object_get_scale (child, &global_child_scale);
  _check_global_scale (child, 0.5f, 0.4f, 0.3f);

  // scaling shouldn't change the poses when everything is at 0,0
  _check_global_position (child, 0, 0, 0);
  _check_global_rotation (child, 0, 0, 0);

  return TRUE;
}

static gboolean
_test_parent_scale_offset (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_local_matrix (parent, &local_transform);

  graphene_point3d_t local_scale = {0.5f, 0.5f, 0.5f};
  g3k_object_set_local_scale (parent, &local_scale);

  graphene_matrix_t child_transform = _make_translation_matrix (8, 10, 12);
  g3k_object_set_local_matrix (child, &child_transform);

  _check_global_position (child, 5, 7, 9);
  _check_global_rotation (child, 0, 0, 0);

  return TRUE;
}

static gboolean
_test_parent_scale_rotate (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_rotation_matrix (0, 90, 0);
  g3k_object_set_local_matrix (parent, &local_transform);

  graphene_point3d_t scale = {0.5f, 0.5f, 0.5f};
  g3k_object_set_local_scale (parent, &scale);

  graphene_matrix_t child_transform = _make_translation_matrix (2, 4, 6);
  g3k_object_set_local_matrix (child, &child_transform);

  _check_global_position (child, 3, 2, -1);
  _check_global_rotation (child, 0, 90, 0);

  return TRUE;
}

static gboolean
_test_g3k_pose (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  (void) parent;

  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t child_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_matrix (child, &child_transform);

  _check_global_position (child, 1, 2, 3);
  _check_global_rotation (child, 0, 0, 0);

  return TRUE;
}

static gboolean
_test_g3k_pose_relative (G3kObject *root, G3kObject *parent, G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_matrix (parent, &local_transform);

  graphene_matrix_t child_transform = _make_translation_matrix (4, 5, 6);
  g3k_object_set_matrix (child, &child_transform);

  return TRUE;
}

static gboolean
_test_g3k_pose_relative_rotation (G3kObject *root,
                                  G3kObject *parent,
                                  G3kObject *child)
{
  g_print ("Test: %s\n", __func__);
  _reset_all (root);

  graphene_matrix_t local_transform = _make_rotation_matrix (0, 90, 0);
  g3k_object_set_local_matrix (parent, &local_transform);

  graphene_matrix_t child_transform = _make_translation_matrix (1, 2, 3);
  g3k_object_set_local_matrix (child, &child_transform);

  return TRUE;
}

static void
_test_set_global_transform ()
{
  G3kObject *object = g3k_object_new ();

  // clang-format off
  float m[16] = {
    +0.727574f, -0.046446f, +0.680384f, +0.000000f,
    -0.220853f, +0.812468f, +0.241867f, +0.000000f,
    -0.573449f, -0.324470f, +0.573523f, +0.000000f,
    +2.769740f, +2.492541f, -2.140265f, +1.000000f,
  };
  float m2[16] = {
    +0.527033f, +0.218246f, -0.696080f, +0.000000f,
    -0.163887f, +0.926114f, +0.042852f, +0.000000f,
    +0.710689f, +0.196215f, +0.518015f, +0.000000f,
    -4.600538f, -0.035784f, -3.095682f, +1.000000f,
  };
  float m3[16] = {
    -0.710718f, -0.121415f, +0.692921f, +0.000000f,
    -0.121415f, -0.949041f, -0.290825f, +0.000000f,
    +0.692921f, -0.290825f, +0.659759f, +0.000000f,
    -7.132993f, +4.231433f, -7.415701f, +1.000000f,
  };
  // clang-format on

  graphene_matrix_t mat;
  graphene_matrix_init_from_float (&mat, m);
  g3k_object_set_matrix (object, &mat);

  graphene_matrix_init_from_float (&mat, m2);
  g3k_object_set_matrix (object, &mat);

  graphene_matrix_init_from_float (&mat, m3);
  g3k_object_set_matrix (object, &mat);
}

int
main ()
{
  G3kObject *root = g3k_object_new ();

  G3kObject *window_root = g3k_object_new ();
  g3k_object_add_child (root, window_root, 0);

  G3kObject *window = g3k_object_new ();
  g3k_object_add_child (window_root, window, 0);

  G3kObject *child_window = g3k_object_new ();
  g3k_object_add_child (window, child_window, 0);

  _check_global_position (child_window, 0, 0, 0);
  _check_global_rotation (child_window, 0, 0, 0);

  _test_child_offset (root, child_window);
  _test_parent_offset (root, window, child_window);
  _test_double_offset (root, window, child_window);
  _test_child_rotation (root, child_window);
  _test_parent_rotation (root, window, child_window);
  _test_parent_rotation_parent_offset (root, window, child_window);
  _test_parent_rotation_child_offset (root, window, child_window);
  _test_parent_neg_rotation_child_offset (root, window, child_window);

  _test_global_scale (root, window, child_window);

  _test_parent_scale (root, window, child_window);
  _test_parent_scale_offset (root, window, child_window);
  _test_parent_scale_rotate (root, window, child_window);

  _test_g3k_pose (root, window, child_window);
  _test_g3k_pose_relative (root, window, child_window);
  _test_g3k_pose_relative_rotation (root, window, child_window);

  _test_set_global_transform ();

  g_print ("passed all tests\n");

  return 0;
}
